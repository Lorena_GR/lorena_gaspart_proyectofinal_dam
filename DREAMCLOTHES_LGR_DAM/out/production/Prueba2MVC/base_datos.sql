CREATE DATABASE IF NOT EXISTS dream_clothes;
USE dream_clothes;

CREATE TABLE IF NOT EXISTS login(
id int primary key auto_increment,
usuario varchar(30),
contrasenia varchar(30),
tipo_cuenta varchar(30)
);

INSERT INTO login (usuario, contrasenia, tipo_cuenta) VALUES
('admin', 'admin', 'Administrador'),
('pepe', 'pepe', 'Trabajador'),
('lorena', 'lorena', 'Cliente');

CREATE TABLE IF NOT EXISTS carrito(
id int primary key auto_increment,
nombre varchar (50),
talla int,
color varchar (50),
tipo varchar (50),
precio float
);

CREATE TABLE IF NOT EXISTS empresa(
id int primary key auto_increment,
nombre varchar(50),
ciudad_sede varchar(50),
cantidad_tiendas int
);

INSERT INTO empresa (nombre, ciudad_sede, cantidad_tiendas) VALUES
('Inditex', 'A Coruña', 6009),
('Mango', 'Barcelona', 2600),
('Grupo Cortefiel', 'Madrid', 1900),
('Desigual', 'Barcelona', 330),
('Pepe Jeans', 'Barcelona', 300),
('Tous', 'Barcelona', 400),
('Blanco', 'Madrid', 240),
('Textil Lonia', 'Ourense', 170),
('Camper', 'Mallorca', 150),
('Punt ROMA', 'Barcelona', 200),
('Adolfo Domínguez', 'Ourense', 695),
('Marypaz', 'Sevilla', 400),
('Liwe Española', 'Murcia', 294),
('Shana', 'Barcelona', 200),
('Bimba & Lola', 'Vigo', 160);


CREATE TABLE IF NOT EXISTS diseniador(
id int primary key auto_increment,
nombre varchar(40),
apellidos varchar(50),
fecha_nacimiento date,
dni varchar (9),
direccion varchar(50),
genero varchar(50),
pais_nacimiento varchar(50),
id_empresa int,
FOREIGN KEY (id_empresa) REFERENCES empresa(id) ON UPDATE CASCADE
);

INSERT INTO diseniador (nombre, apellidos, fecha_nacimiento, dni, direccion, genero, pais_nacimiento, id_empresa) VALUES
('Lorena', 'Gaspart Reyes', '1999-12-02', '73429409T', 'Calle Alicante 20', 'Femenino', 'Bélgica', 4),
('Pepe', 'Rodriguez Padilla', '1975-05-13', '64323421B', 'Calle de Serrano 13', 'Masculino', 'España', 3),
('Martina', 'San Garcia', '1996-11-25', '75342312Q', 'Calle Adelfa 15', 'Prefiere no decirlo', 'España', 7),
('Roma', 'Gracia', '1987-08-25', '73212345A', 'Avenida Estación 2', 'Masculino', 'España', 9),
('Laura', 'Perez', '2000-01-31', '85123194T', 'Calle el Bosque 32', 'Femenino', 'Francia', 12),
('Rosa', 'Gonzalez', '1991-03-22', '76321304R', 'Camino Fuente de los Caños 21', 'Femenino', 'España', 3),
('Pedro', 'Martinez', '1978-06-13', '75839203U', 'Calle Ferrocarril 29', 'Masculino', 'Rusia', 4),
('Pablo', 'Grillote', '1998-07-21', '74578989G', 'Calle de Granada 6', 'Masculino', 'España', 6),
('Marina', 'Pedroche', '1994-12-21', '75849389T', 'Calle de los Siete Picos 9', 'Femenino', 'España', 9),
('Irene', 'Garcia Velazquez', '1965-07-04', '75849310T', 'Calle de Huesca 1', 'Femenino', 'España', 8),
('Roberto', 'Gutierrez', '1990-05-25', '75849315T','Camino Fuente de los Caños 12', 'Masculino', 'España', 1);

CREATE TABLE IF NOT EXISTS coleccion(
id int primary key auto_increment,
nombre varchar(50),
marca varchar(50),
estacion varchar(20),
fecha_creacion date,
id_diseniador int,
FOREIGN KEY (id_diseniador) REFERENCES diseniador(id) ON UPDATE cascade
);

INSERT INTO coleccion (nombre, marca, estacion, fecha_creacion, id_diseniador) VALUES
('Coleccion 1', 'Ermenegildo Zegna', 'Verano', '2020-12-01', 1),
('Coleccion 2', 'Suitman', 'Primavera', '2019-11-25', 1),
('Coleccion 3', 'Atelier', 'Otoño', '2018-04-30', 3),
('Coleccion 4', 'Rosa Clara', 'Verano', '2017-11-21', 7),
('Coleccion 5', 'Matilde Cano', 'Otoño', '2021-07-30', 11),
('Coleccion 6', 'Brook Brothers', 'Primavera', '2022-01-22', 10),
('Coleccion 7', 'Ermenegildo Zegna', 'Otoño', '2019-05-25', 2),
('Coleccion 8', 'Hugo Boss', 'Verano', '2020-10-21', 9),
('Coleccion 9', 'Rosa Clara', 'Invierno', '2019-04-01', 7),
('Coleccion 10', 'VanderWilde', 'Primavera', '2015-12-20', 8),
('Coleccion 11', 'Bimani 13', 'Verano', '2011-11-01', 2),
('Coleccion 12', 'Brook Brothers', 'Primavera', '2021-03-29', 3),
('Coleccion 13', 'Ermenegildo Zegna', 'Invierno', '2020-08-25', 6),
('Coleccion 14', 'Lady Pipa', 'Verano', '2019-01-05', 5),
('Coleccion 15', 'Puro Ego', 'Primavera', '2021-12-01', 5),
('Coleccion 16', 'Rosa Clara', 'Verano', '2022-01-01', 8),
('Coleccion 17', 'BGO and Me', 'Otoño', '2019-03-14', 8),
('Coleccion 18', 'Brook Brothers', 'Invierno', '2021-05-30', 9),
('Coleccion 19', 'Puro Ego', 'Verano', '2019-11-29', 10),
('Coleccion 20', 'Lady Pipa', 'Invierno', '2020-12-04', 11),
('Coleccion 21', 'Rosa Clara', 'Primavera', '2020-11-01', 1),
('Coleccion 22', 'Vogana', 'Otoño', '2019-11-06', 7),
('Coleccion 23', 'Bimani 13', 'Verano', '2018-12-01', 4),
('Coleccion 24', 'Vogana', 'Primavera', '2022-03-15', 4);


CREATE TABLE IF NOT EXISTS prenda(
id int primary key auto_increment,
nombre varchar(50) unique,
persona varchar (20),
tipo varchar(20),
color varchar(20),
talla int,
precio float,
id_coleccion int,
FOREIGN KEY (id_coleccion) REFERENCES coleccion(id) ON UPDATE cascade
);

INSERT INTO prenda (nombre, persona, tipo, color, talla, precio, id_coleccion) VALUES
('Prenda 1', 'Mujer', 'Tacones', 'Negro', 40, 20, 1),
('Prenda 2', 'Hombre', 'Camiseta', 'Azul', 50, 30, 2),
('Prenda 3', 'Hombre', 'Pantalon', 'Amarillo', 38, 21, 5),
('Prenda 4', 'Niño', 'Camiseta', 'Naranja', 23, 30, 2),
('Prenda 5', 'Mujer', 'Tacones', 'Púrpura', 35, 15, 10),
('Prenda 6', 'Niña', 'Falda', 'Azul', 23, 90, 24),
('Prenda 7', 'Hombre', 'Camiseta', 'Negro', 42, 10, 21),
('Prenda 8', 'Mujer', 'Tacones', 'Amarillo', 38, 10, 22),
('Prenda 9', 'Niña', 'Falda', 'Púrpura', 20, 15, 15),
('Prenda 10', 'Mujer', 'Falda', 'Azul', 36, 22, 1),
('Prenda 11', 'Niño', 'Camiseta', 'Naranja', 23, 13, 16),
('Prenda 12', 'Hombre', 'Pantalon', 'Verde', 45, 159, 11),
('Prenda 13', 'Niña', 'Vestido', 'Azul', 22, 24, 21),
('Prenda 14', 'Mujer', 'Zapatos', 'Negro', 35, 56, 6),
('Prenda 15', 'Niña', 'Pantalon', 'Verde', 21, 31, 7),
('Prenda 16', 'Hombre', 'Zapatos', 'Naranja', 46, 11, 8),
('Prenda 17', 'Niño', 'Traje', 'Amarillo', 20, 24, 7),
('Prenda 18', 'Mujer', 'Camiseta', 'Azul', 37, 65, 15),
('Prenda 19', 'Hombre', 'Ropa interior masculina', 'Negro', 49, 76, 16),
('Prenda 20', 'Niña', 'Vestido', 'Negro', 19, 56, 21),
('Prenda 21', 'Hombre', 'Zapatos', 'Púrpura', 40, 54, 22),
('Prenda 22', 'Mujer', 'Tacones', 'Rosa', 35, 34, 3),
('Prenda 23', 'Niño', 'Camiseta', 'Naranja', 20, 123, 4),
('Prenda 24', 'Hombre', 'Pantalon', 'Rosa', 44, 53, 5),
('Prenda 25', 'Mujer', 'Falda', 'Negro', 44, 14, 7);

CREATE TABLE IF NOT EXISTS tienda(
id int primary key auto_increment,
nombre varchar(50),
direccion varchar(100),
ciudad varchar (30),
plantas int
);

INSERT INTO tienda (nombre, direccion, ciudad, plantas) VALUES
('Ghotic clothes', 'C/ Marta del camino 13' , 'Madrid', 1),
('Ashtehic INC', 'Avnd/ Goya 24', 'Málaga', 1),
('Zalando', 'Plaza Europa, 41 – 18, L’hospitalet De Llobregat', 'Barcelona', 3),
('H&M', 'Calle Canuda 45-47 3ª Y 4ª Planta, 08002', 'Barcelona', 4),
('Mango', 'Calle de Serrano, 60', 'Madrid', 2),
('KIABI', 'Centro Comercial Islazul C/ Calderilla, 1, 28054', 'Madrid', 2),
('Stradivarius', 'Centro Comercial Ferial Plaza, Guadalajara', 'Guadalajara', 1),
('Pull & Bear', 'Calle Marqués de Larios, 3, 29015', 'Málaga', 1),
('Bershka', 'C.c La Loma, en la Carr. Bailén-Motril, 37, 23009', 'Jaén', 2),
('Desigual', 'La Rambla, 136, 08001', 'Barcelona', 1);

CREATE TABLE IF NOT EXISTS coleccion_tienda(
id int primary key auto_increment,
id_coleccion int,
id_tienda int,
FOREIGN KEY (id_coleccion) REFERENCES coleccion(id) ON UPDATE CASCADE,
FOREIGN KEY (id_tienda) REFERENCES tienda(id) ON UPDATE CASCADE
);

INSERT INTO coleccion_tienda (id_coleccion, id_tienda) VALUES
(10, 9),
(10, 8),
(10, 5),
(24, 3),
(22, 1),
(20, 2),
(19, 4),
(13, 5),
(18, 9),
(10, 3),
(3, 3),
(2, 4),
(1, 7),
(1, 10),
(24, 9),
(18, 8);


CREATE TABLE IF NOT EXISTS cliente(
id int primary key auto_increment,
nombre varchar(30),
apellidos varchar(50),
fecha_nacimiento date,
dni varchar (9),
genero varchar(30),
correo_electronico varchar(70),
id_tienda int,
FOREIGN KEY (id_tienda) REFERENCES tienda(id) ON UPDATE CASCADE
);

INSERT INTO cliente (nombre, apellidos, fecha_nacimiento, dni, genero, correo_electronico, id_tienda) VALUES
('Lorena', 'Gaspart Reyes', '1999-12-02', '73429409T', 'Femenino', 'l@gmail.com', 4),
('Pepe', 'Rodriguez Padilla', '1975-05-13', '64323421B', 'Masculino', 'prp@gmail.com', 3),
('Martina', 'San Garcia', '1996-11-25', '75342312Q', 'Prefiere no decirlo', 'msg@gmail.com', 7),
('Roma', 'Gracia', '1987-08-25', '73212345A', 'Masculino', 'rg@gmail.com', 9),
('Laura', 'Perez', '2000-01-31', '85123194T', 'Femenino','lp@gmail.com', 10),
('Rosa', 'Gonzalez', '1991-03-22', '76321304R', 'Femenino','rg@gmail.com', 3),
('Pedro', 'Martinez', '1978-06-13', '75839203U', 'Masculino','pm@gmail.com', 4),
('Pablo', 'Grillote', '1998-07-21', '74578989G', 'Masculino','pg@gmail.com', 6),
('Marina', 'Pedroche', '1994-12-21', '75849389T', 'Femenino','mp@gmail.com', 9),
('Irene', 'Garcia Velazquez', '1965-07-04', '73429410R', 'Femenino','igv@gmail.com', 8),
('Roberto', 'Gutierrez', '1990-05-25', '73429412R','Masculino', 'rg@gmail.com', 1);

CREATE TABLE IF NOT EXISTS encargo(
id int primary key auto_increment,
cantidad_prendas int,
precio_total float,
fecha_encargo date,
fecha_entrega date,
id_cliente int,
FOREIGN KEY (id_cliente) REFERENCES cliente(id) ON UPDATE CASCADE
);

INSERT INTO encargo (cantidad_prendas, precio_total, fecha_encargo, fecha_entrega, id_cliente) VALUES
(12, 340, '2022-05-01', '2022-05-14', 1),
(11, 200, '2022-04-04', '2022-05-01', 2),
(1, 10, '2022-04-03', '2022-04-08', 3),
(6, 200, '2022-03-02', '2022-03-05', 5),
(2, 34, '2022-02-21', '2022-02-22', 5),
(15, 250, '2022-01-23', '2022-01-24', 6),
(7, 69, '2022-04-15', '2022-04-25', 8),
(2, 30, '2022-03-14', '2022-03-15', 1),
(9, 104, '2022-04-08', '2022-04-14', 9);

CREATE TABLE IF NOT EXISTS trabajador(
id int primary key auto_increment,
nombre varchar(30),
apellidos varchar(50),
fecha_nacimiento date,
dni varchar (9),
genero varchar (30),
cargo varchar(50),
id_tienda int,
FOREIGN KEY (id_tienda) references tienda(id) ON UPDATE cascade
);

INSERT INTO trabajador (nombre, apellidos, fecha_nacimiento, dni, genero, cargo, id_tienda) VALUES
('Lorena', 'Gaspart Reyes', '1999-12-02', '73429409T', 'Femenino', 'Gerente de tienda', 4),
('Pepe', 'Rodriguez Padilla', '1975-05-13', '64323421B', 'Masculino', 'Asesor de ventas', 3),
('Martina', 'San Garcia', '1996-11-25', '75342312Q', 'Prefiere no decirlo', 'Reponedor', 7),
('Roma', 'Gracia', '1987-08-25', '73212345A', 'Masculino', 'Diseñador de espacios', 9),
('Laura', 'Perez', '2000-01-31', '85123194T', 'Femenino','Cajero', 10),
('Rosa', 'Gonzalez', '1991-03-22', '76321304R', 'Femenino','Gerente de tienda', 3),
('Pedro', 'Martinez', '1978-06-13', '75839203U', 'Masculino','Reponedor', 4),
('Pablo', 'Grillote', '1998-07-21', '74578989G', 'Masculino','Asesor de ventas', 6),
('Marina', 'Pedroche', '1994-12-21', '75849389T', 'Femenino','Gerente de departamento', 9),
('Irene', 'Garcia Velazquez', '1965-07-04','73429410R', 'Femenino','Cajero', 8),
('Roberto', 'Gutierrez', '1990-05-25', '73429412R', 'Masculino', 'Cajero', 1);

--
delimiter ||
create function existeNombreEmpresa(f_nombre varchar (50))
returns bit
begin
    declare i int;
    set i = 0;
    while(i<(select max(id)from empresa)) do
    if ((select nombre from empresa where id = (i + 1)) like f_nombre) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
    end; ||
delimiter;

--

delimiter ||
create function existeDniDiseniador(f_dni varchar (50))
returns bit
begin
    declare i int;
    set i = 0;
    while(i<(select max(id)from diseniador)) do
    if ((select dni from diseniador where id = (i + 1)) like f_dni) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
    end; ||
delimiter;

--

delimiter ||
create function existeNombreColeccion(f_nombre varchar (50))
returns bit
begin
    declare i int;
    set i = 0;
    while(i<(select max(id)from coleccion)) do
    if ((select nombre from coleccion where id = (i + 1)) like f_nombre) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
    end; ||
delimiter;

--

delimiter ||
create function existeNombrePrenda(f_nombre varchar (50))
returns bit
begin
    declare i int;
    set i = 0;
    while(i<(select max(id)from prenda)) do
    if ((select nombre from prenda where id = (i + 1)) like f_nombre) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
    end; ||
delimiter;

--

delimiter ||
create function existeNombreTienda(f_nombre varchar (50))
returns bit
begin
    declare i int;
    set i = 0;
    while(i<(select max(id)from tienda)) do
    if ((select nombre from tienda where id = (i + 1)) like f_nombre) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
    end; ||
delimiter;

--

delimiter ||
create function existeDniTrabajador(f_dni varchar (50))
returns bit
begin
    declare i int;
    set i = 0;
    while(i<(select max(id)from trabajador)) do
    if ((select dni from trabajador where id = (i + 1)) like f_dni) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
    end; ||
delimiter;

--

delimiter ||
create function existeDniCliente(f_dni varchar (50))
returns bit
begin
    declare i int;
    set i = 0;
    while(i<(select max(id)from cliente)) do
    if ((select dni from cliente where id = (i + 1)) like f_dni) then return 1;
    end if;
    set i = i + 1;
    end while;
    return 0;
    end; ||
delimiter;

--

