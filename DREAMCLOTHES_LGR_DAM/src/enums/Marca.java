package enums;

public enum Marca {
    /**
     * ENUM DE LA MARCA DE PRENDA
     */
    HOMBRE("---- HOMBRE ----"),
    MRPORTER("Mr Porter"),
    HUGOBOSS("Hugo Boss"),
    ZEGNA("Ermenegildo Zegna"),
    ETRO("Etro"),
    BURBERRY("Burberry"),
    BOGGIMILANO("Boggi Milano"),
    SILBON("Silbon"),
    BROOKBROTHERS("Brook Brothers"),
    SUITMAN("Suitman"),
    PUROEGO("Puro Ego"),
    MUJER("---- MUJER ----"),
    BGOANDME("BGO and Me"),
    LOLALI("Lola Li"),
    MATILDECANO("Matilde Cano"),
    LADYPIPA("Lady Pipa"),
    BLANCASPINA("BlancaSpina"),
    VOGANA("Vogana"),
    ATELIER("Atelier"),
    ROSACLARA("Rosa Clara"),
    BIMANI("Bimani 13"),
    VANDERWILDE("VanderWilde");

    private String marca;

    /**
     * Constructor que inicia la marca
     * @param marca
     */
    Marca(String marca){
        this.marca=marca;
    }

    public String getMarca() {
        return marca;
    }
}
