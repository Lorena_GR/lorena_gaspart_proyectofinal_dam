package enums;

public enum Cargo {
    /**
     * ENUM DE CARGO DEL TRABAJADOR
     */
    GERENTE("Gerente de tienda"),
    ASESOR("Asesor de ventas"),
    DISENIADOR("Diseñador de espacios"),
    GERENTEDEP("Gerente de departamento"),
    DIRECTOR("Director de distrito"),
    CAJERO("Cajero"),
    REPONEDOR("Reponedor");


    private String cargo;

    Cargo(String cargo){
        this.cargo = cargo;
    }

    public String getCargo() {
        return cargo;
    }
}
