package enums;

public enum Tipo {
    /**
     * ENUM DEL TIPO DE PRENDA
     */
    VESTIDO("Vestido"),
    TRAJE("Traje"),
    ZAPATOS("Zapatos"),
    TACONES("Tacones"),
    CAMISETA("Camiseta"),
    FALDA("Falda"),
    PANTALON("Pantalon"),
    ROPA_INTERIOR("Ropa interior masculina"),
    BRAGAS("Bragas"),
    SUJETADOR("Sujetador");

    String tipo;

    Tipo(String tipo){
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

}
