package enums;

public enum Estacion {
    /**
     * ENUM DE ESTACION DE COLECCION
     */
    PRIMAVERA("Primavera"),
    VERANO("Verano"),
    OTONO("Otoño"),
    INVIERNO("Invierno");


    private String estacion;

    /**
     * Constructor que inicia el atributo estacion
     * @param estacion
     */
    Estacion(String estacion){
        this.estacion=estacion;
    }

    public String getEstacion() {
        return estacion;
    }


}
