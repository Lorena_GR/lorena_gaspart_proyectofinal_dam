package enums;

public enum Genero {
    /**
     * ENUM DE GENERO DEL DISEÑADOR
     */
    MASCULINO("Masculino"),
    FEMENINO("Femenino"),
    SECRETO("Prefiere no decirlo");

    private String genero;

    /**
     * Constructor que inicia el atributo genero
     * @param genero
     */
    Genero(String genero){
        this.genero=genero;
    }

    public String getGenero() {
        return genero;
    }
}
