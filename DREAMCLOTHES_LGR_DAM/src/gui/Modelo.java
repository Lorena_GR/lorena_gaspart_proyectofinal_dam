package gui;

import java.sql.*;

public class Modelo {

    Connection conexion;

    /**
     * Metodo que conecta con la base de datos y returna la conexión
     * @return
     */
    public Connection conectar() {
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/dream_clothes", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conexion;
    }
    /**
     * Metodo que desconecta con la base de datos y returna la conexión
     * @return
     */
    public Connection desconectar(){
        try {
            conexion.close();
            conexion = null;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return conexion;
    }

    /**
     * Metodo que hace una query para insertar una cuenta de cliente
     * @param usuario
     * @param contrasenia
     */
    public void insertarCuentaCliente(String usuario, String contrasenia) {
        String sentenciaSql = "INSERT INTO login (usuario, contrasenia, tipo_cuenta)" + "VALUES(?,?,'Cliente')";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, usuario);
            sentencia.setString(2, contrasenia);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    /**
     * Metodo que hace una query para insertar una cuenta de trabajador
     * @param usuario
     * @param contrasenia
     */
    public void insertarCuentaTrabajador(String usuario, String contrasenia) {
        String sentenciaSql = "INSERT INTO login (usuario, contrasenia, tipo_cuenta)" + "VALUES(?,?,'Trabajador')";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, usuario);
            sentencia.setString(2, contrasenia);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo result set que retorna el resultado de la ejecución de una sentencia para colocar los nombre en los registros de una tabla
     * @param contrasenia
     * @return
     * @throws SQLException
     */
    public ResultSet consultarUsuarioPorContrasenia(String contrasenia) throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(usuario) AS 'Usuario', " +
                "concat(contrasenia) AS 'Contraseña', concat(tipo_cuenta) AS 'Tipo' FROM login WHERE contrasenia = ?";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1, contrasenia);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que inserta una empresa mediante una query
     * @param nombre
     * @param ciudad_sede
     * @param cantidad_tiendas
     */
    public void insertarEmpresa(String nombre, String ciudad_sede, int cantidad_tiendas){
        String sentenciaSql = "INSERT INTO empresa (nombre, ciudad_sede, cantidad_tiendas)" + "VALUES(?, ?, ?)";

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, ciudad_sede);
            sentencia.setInt(3, cantidad_tiendas);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que modifica una empresa mediante una query
     * @param nombre
     * @param ciudad_sede
     * @param cantidad_tiendas
     * @param id
     */
    public void modificarEmpresa(String nombre, String ciudad_sede, int cantidad_tiendas, int id){
        String sentenciaSql= "UPDATE empresa SET nombre = ?, ciudad_sede = ?, cantidad_tiendas = ? WHERE id = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, ciudad_sede);
            sentencia.setInt(3, cantidad_tiendas);
            sentencia.setInt(4, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que elimina una empresa mediante una query
     * @param id
     */
    public void eliminarEmpresa(int id){
        String sentenciaSql = "DELETE FROM empresa WHERE id = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas
     * @return
     * @throws SQLException
     */
    public ResultSet consultarEmpresa() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(ciudad_sede) AS 'Sede', " +
                "concat(cantidad_tiendas) AS 'Nº Tiendas' FROM empresa";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas segun una busqueda
     * @param nombre
     * @return
     * @throws SQLException
     */
    public ResultSet consultarEmpresaPorBusqueda(String nombre) throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(ciudad_sede) AS 'Sede', concat(cantidad_tiendas) AS 'Nº Tiendas' " +
                "FROM empresa WHERE nombre = ?";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1, nombre);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que inserta un diseñador mediante una query
     * @param nombre
     * @param apellidos
     * @param fecha_nacimiento
     * @param dni
     * @param direccion
     * @param genero
     * @param pais_nacimiento
     * @param empresa
     */
    public void insertarDiseniador(String nombre, String apellidos, Date fecha_nacimiento, String dni, String direccion, String genero, String pais_nacimiento, String empresa){
        String sentenciaSql = "INSERT INTO diseniador (nombre, apellidos, fecha_nacimiento, dni, direccion, genero, pais_nacimiento, id_empresa)" + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

        int id_empresa = Integer.valueOf(empresa.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, fecha_nacimiento);
            sentencia.setString(4, dni);
            sentencia.setString(5, direccion);
            sentencia.setString(6, genero);
            sentencia.setString(7, pais_nacimiento);
            sentencia.setInt(8, id_empresa);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que modifica un diseñador mediante una query
     * @param nombre
     * @param apellidos
     * @param fecha_nacimiento
     * @param dni
     * @param direccion
     * @param genero
     * @param pais_nacimiento
     * @param empresa
     * @param id
     */
    public void modificarDiseniador(String nombre, String apellidos, Date fecha_nacimiento, String dni, String direccion, String genero, String pais_nacimiento, String empresa, int id){
        String sentenciaSql = "UPDATE diseniador SET nombre = ?, apellidos = ?, fecha_nacimiento = ?, dni = ?, direccion = ?, genero = ?, pais_nacimiento = ?, id_empresa = ? WHERE id = ?";

        int id_empresa = Integer.valueOf(empresa.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, fecha_nacimiento);
            sentencia.setString(4, dni);
            sentencia.setString(5, direccion);
            sentencia.setString(6, genero);
            sentencia.setString(7, pais_nacimiento);
            sentencia.setInt(8, id_empresa);
            sentencia.setInt(9, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que elimina un diseñador mediante una query
     * @param id
     */
    public void eliminarDiseniador(int id){
        String sentenciaSql = "DELETE FROM diseniador WHERE id = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas
     * @return
     * @throws SQLException
     */
    public ResultSet consultarDiseniador() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(apellidos) AS 'Apellidos', concat(fecha_nacimiento) AS 'Fecha de nacimiento', " +
                "concat(dni) AS 'DNI', concat(direccion) AS 'Direccion', concat(genero) AS 'Genero', " +
                "concat(pais_nacimiento) AS 'Pais de nacimiento', concat(id_empresa) AS 'Empresa' FROM diseniador";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas segun un campo
     * @param empresa
     * @return
     * @throws SQLException
     */
    public ResultSet consultarDiseniadorPorBusqueda(String empresa) throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(apellidos) AS 'Apellidos', concat(fecha_nacimiento) AS 'Fecha de nacimiento', " +
                "concat(dni) AS 'DNI', concat(direccion) AS 'Direccion', concat(genero) AS 'Genero', " +
                "concat(pais_nacimiento) AS 'Pais de nacimiento', concat(id_empresa) AS 'Empresa' FROM diseniador WHERE id_empresa = ?";

        int id_empresa = Integer.valueOf(empresa.split(" ")[0]);

        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setInt(1, id_empresa);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que inserta una coleccion mediante una query
     * @param nombre
     * @param marca
     * @param estacion
     * @param fecha_creacion
     * @param diseniador
     */
    public void insertarColeccion(String nombre, String marca, String estacion, Date fecha_creacion, String diseniador){
        String sentenciaSql = "INSERT INTO coleccion (nombre, marca, estacion, fecha_creacion, id_diseniador) VALUES (?, ?, ?, ?, ?)";

        int id_diseniador = Integer.valueOf(diseniador.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, marca);
            sentencia.setString(3, estacion);
            sentencia.setDate(4, fecha_creacion);
            sentencia.setInt(5, id_diseniador);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que modifica una coleccion mediante una query
     * @param nombre
     * @param marca
     * @param estacion
     * @param fecha_creacion
     * @param diseniador
     * @param id
     */
    public void modificarColeccion(String nombre, String marca, String estacion, Date fecha_creacion, String diseniador, int id){
        String sentenciaSql = "UPDATE coleccion SET nombre = ?, marca = ?, estacion = ?, fecha_creacion = ?, id_diseniador = ? WHERE id = ?";

        int id_diseniador = Integer.valueOf(diseniador.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, marca);
            sentencia.setString(3, estacion);
            sentencia.setDate(4, fecha_creacion);
            sentencia.setInt(5, id_diseniador);
            sentencia.setInt(6, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que elimina una coleccion mediante una query
     * @param id
     */
    public void eliminarColeccion(int id){
        String sentenciaSql = "DELETE FROM coleccion WHERE id = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas
     * @return
     * @throws SQLException
     */
    public ResultSet consultarColeccion() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(marca) AS 'Marca', concat(estacion) AS 'Estacion', " +
                "concat(fecha_creacion) AS 'Fecha de creación', concat(id_diseniador) AS 'Diseñador' FROM coleccion";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas por una busqueda
     * @param diseniador
     * @return
     * @throws SQLException
     */
    public ResultSet consultarColeccionPorBusqueda(String diseniador) throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(marca) AS 'Marca', concat(estacion) AS 'Estacion', " +
                "concat(fecha_creacion) AS 'Fecha de creación', concat(id_diseniador) AS 'Diseñador' FROM coleccion WHERE id_diseniador = ?";

        int id_diseniador = Integer.valueOf(diseniador.split(" ")[0]);

        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setInt(1, id_diseniador);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que inserta una prenda mediante una query
     * @param nombre
     * @param persona
     * @param tipo
     * @param color
     * @param talla
     * @param precio
     * @param coleccion
     */
    public void insertarPrenda(String nombre, String persona, String tipo, String color, int talla, float precio, String coleccion){
        String sentenciaSql = "INSERT INTO prenda (nombre, persona, tipo, color, talla, precio, id_coleccion) VALUES (?, ?, ?, ?, ?, ?, ?)";

        int id_coleccion = Integer.valueOf(coleccion.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, persona);
            sentencia.setString(3, tipo);
            sentencia.setString(4, color);
            sentencia.setInt(5, talla);
            sentencia.setFloat(6, precio);
            sentencia.setInt(7, id_coleccion);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que modifica una prenda mediante una query
     * @param nombre
     * @param persona
     * @param tipo
     * @param color
     * @param talla
     * @param precio
     * @param coleccion
     * @param id
     */
    public void modificarPrenda(String nombre, String persona, String tipo, String color, int talla, float precio, String coleccion, int id){
        String sentenciaSql = "UPDATE prenda SET nombre = ?, persona = ?, tipo = ?, color = ?, talla = ?, precio = ?, id_coleccion = ? WHERE id = ?";

        int id_coleccion = Integer.valueOf(coleccion.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, persona);
            sentencia.setString(3, tipo);
            sentencia.setString(4, color);
            sentencia.setInt(5, talla);
            sentencia.setFloat(6, precio);
            sentencia.setInt(7, id_coleccion);
            sentencia.setInt(8, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que elimina una prenda mediante una query
     * @param id
     */
    public void eliminarPrenda(int id){
        String sentenciaSql = "DELETE FROM prenda WHERE id = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas
     * @return
     * @throws SQLException
     */
    public ResultSet consultarPrenda() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', concat(persona) AS 'Persona', " +
                "concat(tipo) AS 'Tipo', concat(color) AS 'Color', " +
                "concat(talla) AS 'Talla', concat(precio) AS 'Precio', " +
                "concat(id_coleccion) AS 'Colección' FROM prenda";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas por una busqueda
     * @param coleccion
     * @return
     * @throws SQLException
     */
    public ResultSet consultarPrendaPorBusqueda(String coleccion) throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', concat(persona) AS 'Persona', " +
                "concat(tipo) AS 'Tipo', concat(color) AS 'Color', " +
                "concat(talla) AS 'Talla', concat(precio) AS 'Precio', " +
                "concat(id_coleccion) AS 'Colección' FROM prenda WHERE id_coleccion = ?";

        int id_empresa = Integer.valueOf(coleccion.split(" ")[0]);

        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setInt(1, id_empresa);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que inserta una tienda mediante una query
     * @param nombre
     * @param direccion
     * @param ciudad
     * @param plantas
     */
    public void insertarTienda(String nombre, String direccion, String ciudad, int plantas){
        String sentenciaSql = "INSERT INTO tienda (nombre, direccion, ciudad, plantas) VALUES (?, ?, ?, ?)";

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setString(3, ciudad);
            sentencia.setInt(4, plantas);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que modifica una tienda mediante una query
     * @param nombre
     * @param direccion
     * @param ciudad
     * @param plantas
     * @param id
     */
    public void modificarTienda(String nombre, String direccion, String ciudad, int plantas, int id){
        String sentenciaSql = "UPDATE tienda SET nombre = ?, direccion = ?, ciudad = ?, plantas = ? WHERE id = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, direccion);
            sentencia.setString(3, ciudad);
            sentencia.setInt(4, plantas);
            sentencia.setInt(5, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que elimine una tienda mediante una query
     * @param id
     */
    public void eliminarTienda(int id){
        String sentenciaSql = "DELETE FROM tienda WHERE id = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas
     * @return
     * @throws SQLException
     */
    public ResultSet consultarTienda() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(direccion) AS 'Direccion', concat(ciudad) AS 'Ciudad', concat(plantas) AS 'Nº de plantas' FROM tienda";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas por busqueda
     * @param nombre
     * @return
     * @throws SQLException
     */
    public ResultSet consultarTiendaPorBusqueda(String nombre) throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(direccion) AS 'Direccion', concat(plantas) AS 'Nº de plantas' FROM tienda WHERE nombre = ?";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1, nombre);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que inserta un registro de la tabla relacionada de tienda y coleccion mediante una query
     * @param coleccion
     * @param tienda
     */
    public void insertarCT(String coleccion, String tienda){
        String sentenciaSql = "INSERT INTO coleccion_tienda (id_coleccion, id_tienda) VALUES (?, ?)";
        int id_coleccion = Integer.valueOf(coleccion.split(" ")[0]);
        int id_tienda = Integer.valueOf(tienda.split(" ")[0]);
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id_coleccion);
            sentencia.setInt(2, id_tienda);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que modifica un registro de la tabla relacionada de tienda y coleccion mediante una query
     * @param coleccion
     * @param tienda
     * @param id
     */
    public void modificarCT(String coleccion, String tienda, int id){
        String sentenciaSql = "UPDATE coleccion_tienda SET id_coleccion = ?, id_tienda = ? WHERE id = ?";
        int id_coleccion = Integer.valueOf(coleccion.split(" ")[0]);
        int id_tienda = Integer.valueOf(tienda.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id_coleccion);
            sentencia.setInt(2, id_tienda);
            sentencia.setInt(3, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que elimina un registro de la tabla relacionada de tienda y coleccion mediante una query
     * @param id
     */
    public void eliminarCT(int id){
        String sentenciaSql = "DELETE FROM coleccion_tienda WHERE id = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas
     * @return
     * @throws SQLException
     */
    public ResultSet consultarCT() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(id_coleccion) AS 'Colección', concat(id_tienda) AS 'Tienda' FROM coleccion_tienda";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas por busqueda
     * @param tienda
     * @return
     * @throws SQLException
     */
    public ResultSet consultarCTPorBusqueda(String tienda) throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(id_coleccion) AS 'Colección', concat(id_tienda) AS 'Tienda' FROM coleccion_tienda WHERE id_tienda = ?";

        int id_tienda = Integer.valueOf(tienda.split(" ")[0]);

        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setInt(1, id_tienda);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que inserta un trabajador mediante una query
     * @param nombre
     * @param apellidos
     * @param fecha_nacimiento
     * @param dni
     * @param genero
     * @param cargo
     * @param tienda
     */
    public void insertarTrabajador(String nombre, String apellidos, Date fecha_nacimiento, String dni, String genero, String cargo, String tienda){
        String sentenciaSql = "INSERT INTO trabajador (nombre, apellidos, fecha_nacimiento, dni, genero, cargo, id_tienda) VALUES (?, ?, ?, ?, ?, ?, ?)";

        int id_tienda = Integer.valueOf(tienda.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, fecha_nacimiento);
            sentencia.setString(4, dni);
            sentencia.setString(5, genero);
            sentencia.setString(6, cargo);
            sentencia.setInt(7, id_tienda);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Metodo que modifica un trabajador mediante una query
     * @param nombre
     * @param apellidos
     * @param fecha_nacimiento
     * @param dni
     * @param genero
     * @param cargo
     * @param tienda
     * @param id
     */
    public void modificarTrabajador(String nombre, String apellidos, Date fecha_nacimiento, String dni, String genero, String cargo, String tienda, int id){
        String sentenciaSql = "UPDATE trabajador SET nombre = ?, apellidos = ?, fecha_nacimiento = ?, dni = ?, genero = ?, cargo = ?, id_tienda = ? WHERE id = ?";

        int id_tienda = Integer.valueOf(tienda.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, fecha_nacimiento);
            sentencia.setString(4, dni);
            sentencia.setString(5, genero);
            sentencia.setString(6, cargo);
            sentencia.setInt(7, id_tienda);
            sentencia.setInt(8, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que elimina un trabajador mediante una query
     * @param id
     */
    public void eliminarTrabajador(int id){
        String sentenciaSql = "DELETE FROM trabajador WHERE id = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas
     * @return
     * @throws SQLException
     */
    public ResultSet consultarTrabajador() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(apellidos) AS 'Apellidos', concat(fecha_nacimiento) AS 'Fecha de nacimiento', " +
                "concat(dni) AS 'DNI', concat(genero) AS 'Genero', concat(cargo) AS 'Cargo', " +
                "concat(id_tienda) AS 'Tienda' FROM trabajador";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas por busqueda
     * @param tienda
     * @return
     * @throws SQLException
     */
    public ResultSet consultarTrabajadorPorBusqueda(String tienda) throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(apellidos) AS 'Apellidos', concat(fecha_nacimiento) AS 'Fecha de nacimiento', " +
                "concat(dni) AS 'DNI', concat(genero) AS 'Genero', concat(cargo) AS 'Cargo', " +
                "concat(id_tienda) AS 'Tienda' FROM trabajador WHERE id_tienda = ?";

        int id_tienda = Integer.valueOf(tienda.split(" ")[0]);

        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setInt(1, id_tienda);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que inserta un cliente mediante una query
     * @param nombre
     * @param apellidos
     * @param fecha_nacimiento
     * @param dni
     * @param genero
     * @param correo_electronico
     * @param tienda
     */
    public void insertarCliente(String nombre, String apellidos, Date fecha_nacimiento, String dni, String genero, String correo_electronico, String tienda){
        String sentenciaSql = "INSERT INTO cliente (nombre, apellidos, fecha_nacimiento, dni, genero, correo_electronico, id_tienda) VALUES (?, ?, ?, ?, ?, ?, ?)";

        int id_tienda = Integer.valueOf(tienda.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, fecha_nacimiento);
            sentencia.setString(4, dni);
            sentencia.setString(5, genero);
            sentencia.setString(6, correo_electronico);
            sentencia.setInt(7, id_tienda);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que modifica un cliente mediante una query
     * @param nombre
     * @param apellidos
     * @param fecha_nacimiento
     * @param dni
     * @param genero
     * @param correo_electronico
     * @param tienda
     * @param id
     */
    public void modificarCliente(String nombre, String apellidos, Date fecha_nacimiento, String dni, String genero, String correo_electronico, String tienda, int id){
        String sentenciaSql = "UPDATE cliente SET nombre = ?, apellidos = ?, fecha_nacimiento = ?, dni = ?, genero = ?, correo_electronico = ?, id_tienda = ? WHERE id = ?";

        int id_tienda = Integer.valueOf(tienda.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setString(2, apellidos);
            sentencia.setDate(3, fecha_nacimiento);
            sentencia.setString(4, dni);
            sentencia.setString(5, genero);
            sentencia.setString(6, correo_electronico);
            sentencia.setInt(7, id_tienda);
            sentencia.setInt(8, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que elimina un cliente mediante una query
     * @param id
     */
    public void eliminarCliente(int id){
        String sentenciaSql = "DELETE FROM cliente WHERE id = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas
     * @return
     * @throws SQLException
     */
    public ResultSet consultarCliente() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(apellidos) AS 'Apellidos', concat(fecha_nacimiento) AS 'Fecha de nacimiento', " +
                "concat(dni) AS 'DNI', concat(genero) AS 'Genero', concat(correo_electronico) AS 'Correo', " +
                "concat(id_tienda) AS 'Tienda' FROM cliente";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas por busqueda
     * @param tienda
     * @return
     * @throws SQLException
     */
    public ResultSet consultarClientePorBusqueda(String tienda) throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', " +
                "concat(apellidos) AS 'Apellidos', concat(fecha_nacimiento) AS 'Fecha de nacimiento', " +
                "concat(DNI) AS 'DNI', concat(genero) AS 'Genero', concat(correo_electronico) AS 'Correo', " +
                "concat(id_tienda) AS 'Tienda' FROM cliente WHERE id_tienda = ?";

        int id_tienda = Integer.valueOf(tienda.split(" ")[0]);

        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setInt(1, id_tienda);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que inserta un encargo mediante una query
     * @param cantidad_prendas
     * @param precio_total
     * @param fecha_encargo
     * @param fecha_entrega
     * @param cliente
     */
    public void insertarEncargo(int cantidad_prendas, float precio_total, Date fecha_encargo, Date fecha_entrega, String cliente){
        String  sentenciaSql = "INSERT INTO encargo (cantidad_prendas, precio_total, fecha_encargo, fecha_entrega, id_cliente) VALUES (?, ?, ?, ?, ?)";

        int id_cliente = Integer.valueOf(cliente.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, cantidad_prendas);
            sentencia.setFloat(2, precio_total);
            sentencia.setDate(3, fecha_encargo);
            sentencia.setDate(4, fecha_entrega);
            sentencia.setInt(5, id_cliente);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que modifica un encargo mediante una query
     * @param cantidad_prendas
     * @param precio_total
     * @param fecha_encargo
     * @param fecha_entrega
     * @param cliente
     * @param id
     */
    public void modificarEncargo(int cantidad_prendas, float precio_total, Date fecha_encargo, Date fecha_entrega, String cliente, int id){
        String  sentenciaSql = "UPDATE encargo SET cantidad_prendas = ?, precio_total = ?, fecha_encargo = ?, fecha_entrega = ?, id_cliente = ? WHERE id = ?";

        int id_cliente = Integer.valueOf(cliente.split(" ")[0]);

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, cantidad_prendas);
            sentencia.setFloat(2, precio_total);
            sentencia.setDate(3, fecha_encargo);
            sentencia.setDate(4, fecha_entrega);
            sentencia.setInt(5, id_cliente);
            sentencia.setInt(6, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que elimina un encargo mediante una query
     * @param id
     */
    public void eliminarEncargo(int id) {
        String sentenciaSql = "DELETE FROM encargo WHERE id = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, id);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que inserta una prenda al carrito mediante una query
     * @param nombre
     * @param talla
     * @param color
     * @param tipo
     * @param precio
     */
    public void insertarPrendaCarrito(String nombre, int talla, String color, String tipo, float precio){
        String  sentenciaSql = "INSERT INTO carrito (nombre, talla, color, tipo, precio) VALUES (?, ?, ?, ?, ?)";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.setInt(2, talla);
            sentencia.setString(3, color);
            sentencia.setString(4, tipo);
            sentencia.setFloat(5, precio);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que elimna una prenda del carrito mediante una query
     * @param nombre
     */
    public void eliminarPrendaCarrito(String nombre){
        String sentenciaSql = "DELETE FROM carrito WHERE nombre = ?";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, nombre);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * metodo que vacia el carrito entero
     */
    public void eliminarCarrito(){
        String sentenciaSql = "DELETE FROM carrito";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas
     * @return
     * @throws SQLException
     */
    public ResultSet consultarEncargo() throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(cantidad_prendas) AS 'Cantidad de prendas', " +
                "concat(precio_total) AS 'Precio total', concat(fecha_encargo) AS 'Fecha de encargo', " +
                "concat(fecha_entrega) AS 'Fecha de entrega', concat(id_cliente) AS 'Cliente' FROM encargo";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     *  Metodo que retorna un result set al ejecutar una query para mostrar los nombres en las tablas por busqueda
     * @param cliente
     * @return
     * @throws SQLException
     */
    public ResultSet consultarEncargoPorBusqueda(String cliente) throws SQLException {
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(cantidad_prendas) AS 'Cantidad de prendas', " +
                "concat(precio_total) AS 'Precio total', concat(fecha_encargo) AS 'Fecha de encargo', " +
                "concat(fecha_entrega) AS 'Fecha de entrega', concat(id_cliente) AS 'Cliente' FROM encargo WHERE id_cliente = ?";

        int id_cliente = Integer.valueOf(cliente.split(" ")[0]);

        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setInt(1, id_cliente);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    // CLIENTE

    /**
     * Metodo result set que filtra por tipo
     * @param tipo
     * @return
     * @throws SQLException
     */
    public ResultSet filtrar(String tipo) throws SQLException{
        String sentenciaSql = "SELECT concat(id) AS 'ID', concat(nombre) AS 'Nombre', concat(persona) AS 'Persona', " +
                "concat(tipo) AS 'Tipo', concat(color) AS 'Color', " +
                "concat(talla) AS 'Talla', concat(precio) AS 'Precio', " +
                "concat(id_coleccion) AS 'Colección' FROM prenda WHERE tipo = ?";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1, tipo);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    // CARRITO

    /**
     * Metodo result set que consulta un carrito añadiendo los nombres de los campos a la tabla
     * @return
     * @throws SQLException
     */
    public ResultSet consultarCarrito() throws SQLException {
        String sentenciaSql = "SELECT concat(nombre) AS 'Nombre', concat(talla) AS 'Talla', " +
                "concat(color) AS 'Color', concat(tipo) AS 'Tipo', " +
                "concat(precio) AS 'Precio' FROM carrito";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSql);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }



    // FUNCIONES

    /**
     * Metodo que llama una función para ver si el nombre  de la empresa que se va añadir ya existe
     * @param nombre
     * @return
     */
    public boolean nombreEmpresaYaExiste(String nombre){
        String nombreEmpresa = "SELECT existeNombreEmpresa(?)";
        PreparedStatement funcion;
        boolean existe = false;
        try {
            funcion = conexion.prepareCall(nombreEmpresa);
            funcion.setString(1, nombre);
            ResultSet set = funcion.executeQuery();
            set.next();
            existe = set.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;
    }

    /**
     * Metodo que llama una función para ver si el dni  del diseñador que se va añadir ya existe
     * @param dni
     * @return
     */
    public boolean dniDiseniadorYaExiste(String dni){
        String dniDiseniador = "SELECT existeDniDiseniador(?)";
        PreparedStatement funcion;
        boolean existe = false;
        try {
            funcion = conexion.prepareCall(dniDiseniador);
            funcion.setString(1, dni);
            ResultSet set = funcion.executeQuery();
            set.next();
            existe = set.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;
    }

    /**
     * Metodo que llama una función para ver si el nombre  de la coleccion que se va añadir ya existe
     * @param nombre
     * @return
     */
    public boolean nombreColeccionYaExiste(String nombre){
        String nombreColeccion = "SELECT existeNombreColeccion(?)";
        PreparedStatement funcion;
        boolean existe = false;
        try {
            funcion = conexion.prepareCall(nombreColeccion);
            funcion.setString(1, nombre);
            ResultSet set = funcion.executeQuery();
            set.next();
            existe = set.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;
    }

    /**
     * Metodo que llama una función para ver si el nombre  de la prenda que se va añadir ya existe
     * @param nombre
     * @return
     */
    public boolean nombrePrendaYaExiste(String nombre){
        String nombrePrenda = "SELECT existeNombrePrenda(?)";
        PreparedStatement funcion;
        boolean existe = false;
        try {
            funcion = conexion.prepareCall(nombrePrenda);
            funcion.setString(1, nombre);
            ResultSet set = funcion.executeQuery();
            set.next();
            existe = set.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;
    }

    /**
     * Metodo que llama una función para ver si el nombre  de la tienda que se va añadir ya existe
     * @param nombre
     * @return
     */
    public boolean nombreTiendaYaExiste(String nombre){
        String nombreTienda = "SELECT existeNombreTienda(?)";
        PreparedStatement funcion;
        boolean existe = false;
        try {
            funcion = conexion.prepareCall(nombreTienda);
            funcion.setString(1, nombre);
            ResultSet set = funcion.executeQuery();
            set.next();
            existe = set.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;
    }

    /**
     * Metodo que llama una función para ver si el dni  del trabajador que se va añadir ya existe
     * @param dni
     * @return
     */
    public boolean dniTrabajadorYaExiste(String dni){
        String dniTrabajador = "SELECT existeDniTrabajador(?)";
        PreparedStatement funcion;
        boolean existe = false;
        try {
            funcion = conexion.prepareCall(dniTrabajador);
            funcion.setString(1, dni);
            ResultSet set = funcion.executeQuery();
            set.next();
            existe = set.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;
    }

    /**
     *  Metodo que llama una función para ver si el dni  del cliente que se va añadir ya existe
     * @param dni
     * @return
     */
    public boolean dniClienteYaExiste(String dni){
        String dniCliente = "SELECT existeDniCliente(?)";
        PreparedStatement funcion;
        boolean existe = false;
        try {
            funcion = conexion.prepareCall(dniCliente);
            funcion.setString(1, dni);
            ResultSet set = funcion.executeQuery();
            set.next();
            existe = set.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return existe;
    }

}
