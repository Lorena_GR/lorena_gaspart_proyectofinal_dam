package gui;

import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.net.URL;

public class Inicio_Sesion extends JFrame {
    private final static String TITULOFRAME = "DREAM CLOTHES";
    private JPanel panel1;
     JTextField txtUsuario;
     JPasswordField txtPass;
     JButton btnIniciarCliente;
     JButton btnCrearCliente;
     JButton btnIniciarTrabajador;
     JButton btnIniciarAdmin;
     JLabel Imagen;
     JMenuItem principal;
     JMenuItem manual_usuario;
    /**
     * Constructor de la ventana inicio de sesión donde inicias el frame y le pones el titulo.
     */
    public Inicio_Sesion(){
        super(TITULOFRAME);
        initFrame();
    }
    /**
     * Metodo que crea todos los componentes de la ventana, tanto tamaño, localización la imagen del icono...
     */
    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(this);
        this.setIconImage(new ImageIcon("DREAM.png").getImage());
        this.setLocation(750, 250);
        this.setResizable(false);
        setMenu();
    }

    /**
     * Metodo que pone la imagen del logo en la ventana
     */
    private void createUIComponents() {
        Imagen = new JLabel(new ImageIcon("DREAM.png"));
    }

    private void setMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("MANUALES");
        principal = new JMenuItem("Principal");
        principal.setActionCommand("Principal");
        manual_usuario = new JMenuItem("Manual");
        manual_usuario.setActionCommand("Manual");
        menu.add(principal);
        menu.add(manual_usuario);
        barra.add(menu);
        this.setJMenuBar(barra);
    }

    public void ponLaAyuda() {
        try {
            // Carga el fichero de ayuda
            File fichero = new File("help_set.hs");
            //File fichero = new File("src"+File.separator+"pruebajavahelp\\help\\help_set.hs");
            URL hsURL = fichero.toURI().toURL();

            // Crea el HelpSet y el HelpBroker
            HelpSet helpset = new HelpSet(getClass().getClassLoader(), hsURL);
            HelpBroker hb = helpset.createHelpBroker();

            // Pone ayuda a item de menu al pulsarlo y a F1 en ventana
            // manual e instalar.
            hb.enableHelpOnButton(principal, "aplicacion", helpset);
            hb.enableHelpOnButton(manual_usuario, "manual", helpset);
            hb.enableHelpKey(panel1, "aplicacion", helpset);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
