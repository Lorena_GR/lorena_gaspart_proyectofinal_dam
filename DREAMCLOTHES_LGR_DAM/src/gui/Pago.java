package gui;

import javax.swing.*;
import java.awt.*;

public class Pago extends JFrame{
    private JPanel panel1;
    /**
     * Constructor de la ventana pago donde inicias el frame
     */
    Pago(){
        initFrame();
    }

    /**
     * Metodo que crea todos los componentes de la ventana, tanto tamaño, localización la imagen del icono...
     */
    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(300, 100));
        this.setLocationRelativeTo(this);
        this.setLocation(750, 250);
        this.setIconImage(new ImageIcon("DREAM.png").getImage());
        this.setResizable(false);
        this.setDefaultCloseOperation(this.HIDE_ON_CLOSE);
    }
}
