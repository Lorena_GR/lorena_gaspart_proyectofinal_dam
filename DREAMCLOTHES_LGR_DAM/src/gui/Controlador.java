package gui;

import Util.Util;
import Util.Informes;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.*;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    private Modelo modelo;
    private Inicio_Sesion inicio_sesion;
    private Ventana_Administrador ventana_administrador = new Ventana_Administrador();
    private Ventana_Cliente ventana_cliente = new Ventana_Cliente();
    private Ventana_Trabajador ventana_trabajador = new Ventana_Trabajador();
    private Carrito carrito = new Carrito();
    private Pago pago = new Pago();
    boolean refrescar;
    static float precioTotal = 0;

    /**
     * En el constructor del controlador hacemos que las ventanas no sean visibles. Hacemos que se conecte con la base de datos, refrescamos todo para que salgan los registros
     * y añadimos los listeners de action y window.
     * @param inicio_sesion
     * @param modelo
     */
    public Controlador(Inicio_Sesion inicio_sesion, Modelo modelo){
        this.modelo = modelo;
        this.inicio_sesion = inicio_sesion;
        ventana_administrador.setVisible(false);
        ventana_cliente.setVisible(false);
        ventana_trabajador.setVisible(false);
        carrito.setVisible(false);
        pago.setVisible(false);
        modelo.conectar();
        addActionListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    /**
     * En este metodo activamos todos los botones que tengamos para nuestra aplicación y además les damos nombre de comando para el switch
     * @param listener
     */
    private void addActionListeners(ActionListener listener){
        // LOGINS
        ventana_administrador.cerrarSesion.addActionListener(listener);
        ventana_cliente.cerrarSesion.addActionListener(listener);
        ventana_trabajador.cerrarSesion.addActionListener(listener);
        inicio_sesion.btnCrearCliente.addActionListener(listener);
        inicio_sesion.btnCrearCliente.setActionCommand("Crear_cliente");
        inicio_sesion.btnIniciarCliente.addActionListener(listener);
        inicio_sesion.btnIniciarCliente.setActionCommand("Iniciar_cliente");
        inicio_sesion.btnIniciarAdmin.addActionListener(listener);
        inicio_sesion.btnIniciarAdmin.setActionCommand("Iniciar_admin");
        inicio_sesion.btnIniciarTrabajador.addActionListener(listener);
        inicio_sesion.btnIniciarTrabajador.setActionCommand("Iniciar_trabajador");
        ventana_administrador.btnCrearTrabajador.addActionListener(listener);
        ventana_administrador.btnCrearTrabajador.setActionCommand("Crear_trabajador");
        // BOTONES AÑADIR
        ventana_trabajador.btnAnyadirEmpresa.addActionListener(listener);
        ventana_trabajador.btnAnyadirEmpresa.setActionCommand("Anyadir_empresa");
        ventana_trabajador.btnAnyadirDisen.addActionListener(listener);
        ventana_trabajador.btnAnyadirDisen.setActionCommand("Anyadir_disen");
        ventana_trabajador.btnAnyadirColeccion.addActionListener(listener);
        ventana_trabajador.btnAnyadirColeccion.setActionCommand("Anyadir_coleccion");
        ventana_trabajador.btnAnyadirPrenda.addActionListener(listener);
        ventana_trabajador.btnAnyadirPrenda.setActionCommand("Anyadir_prenda");
        ventana_trabajador.btnAnyadirTienda.addActionListener(listener);
        ventana_trabajador.btnAnyadirTienda.setActionCommand("Anyadir_tienda");
        ventana_trabajador.btnAniadirColTienda.addActionListener(listener);
        ventana_trabajador.btnAniadirColTienda.setActionCommand("Anyadir_CT");
        ventana_trabajador.btnAnyadirTrabajador.addActionListener(listener);
        ventana_trabajador.btnAnyadirTrabajador.setActionCommand("Anyadir_trabajador");
        ventana_trabajador.btnAnyadirCliente.addActionListener(listener);
        ventana_trabajador.btnAnyadirCliente.setActionCommand("Anyadir_cliente");
        ventana_trabajador.btnAnyadirEncargo.addActionListener(listener);
        ventana_trabajador.btnAnyadirEncargo.setActionCommand("Anyadir_encargo");
        // BOTONES MODIFICAR
        ventana_trabajador.btnModificarEmpresa.addActionListener(listener);
        ventana_trabajador.btnModificarEmpresa.setActionCommand("Modificar_empresa");
        ventana_trabajador.btnModificarDisen.addActionListener(listener);
        ventana_trabajador.btnModificarDisen.setActionCommand("Modificar_disen");
        ventana_trabajador.btnModificarColeccion.addActionListener(listener);
        ventana_trabajador.btnModificarColeccion.setActionCommand("Modificar_coleccion");
        ventana_trabajador.btnModificarPrenda.addActionListener(listener);
        ventana_trabajador.btnModificarPrenda.setActionCommand("Modificar_prenda");
        ventana_trabajador.btnModificarTienda.addActionListener(listener);
        ventana_trabajador.btnModificarTienda.setActionCommand("Modificar_tienda");
        ventana_trabajador.btnModificarColTienda.addActionListener(listener);
        ventana_trabajador.btnModificarColTienda.setActionCommand("Modificar_CT");
        ventana_trabajador.btnModificarTrabajador.addActionListener(listener);
        ventana_trabajador.btnModificarTrabajador.setActionCommand("Modificar_trabajador");
        ventana_trabajador.btnModificarCliente.addActionListener(listener);
        ventana_trabajador.btnModificarCliente.setActionCommand("Modificar_cliente");
        ventana_trabajador.btnModificarEncargo.addActionListener(listener);
        ventana_trabajador.btnModificarEncargo.setActionCommand("Modificar_encargo");
        // BOTONES ELIMINAR
        ventana_trabajador.btnEliminarEmpresa.addActionListener(listener);
        ventana_trabajador.btnEliminarEmpresa.setActionCommand("Eliminar_empresa");
        ventana_trabajador.btnEliminarDisen.addActionListener(listener);
        ventana_trabajador.btnEliminarDisen.setActionCommand("Eliminar_disen");
        ventana_trabajador.btnEliminarColeccion.addActionListener(listener);
        ventana_trabajador.btnEliminarColeccion.setActionCommand("Eliminar_coleccion");
        ventana_trabajador.btnEliminarPrenda.addActionListener(listener);
        ventana_trabajador.btnEliminarPrenda.setActionCommand("Eliminar_prenda");
        ventana_trabajador.btnEliminarTienda.addActionListener(listener);
        ventana_trabajador.btnEliminarTienda.setActionCommand("Eliminar_tienda");
        ventana_trabajador.btnEliminarColTienda.addActionListener(listener);
        ventana_trabajador.btnEliminarColTienda.setActionCommand("Eliminar_CT");
        ventana_trabajador.btnEliminarTrabajador.addActionListener(listener);
        ventana_trabajador.btnEliminarTrabajador.setActionCommand("Eliminar_trabajador");
        ventana_trabajador.btnEliminarCliente.addActionListener(listener);
        ventana_trabajador.btnEliminarCliente.setActionCommand("Eliminar_cliente");
        ventana_trabajador.btnEliminarEncargo.addActionListener(listener);
        ventana_trabajador.btnEliminarEncargo.setActionCommand("Eliminar_encargo");
        // BOTONES BUSCAR
        ventana_trabajador.btnBuscarEmpresa.addActionListener(listener);
        ventana_trabajador.btnBuscarEmpresa.setActionCommand("Buscar_empresa");
        ventana_trabajador.btnBuscarDisen.addActionListener(listener);
        ventana_trabajador.btnBuscarDisen.setActionCommand("Buscar_disen");
        ventana_trabajador.btnBuscarColeccion.addActionListener(listener);
        ventana_trabajador.btnBuscarColeccion.setActionCommand("Buscar_coleccion");
        ventana_trabajador.btnBuscarPrenda.addActionListener(listener);
        ventana_trabajador.btnBuscarPrenda.setActionCommand("Buscar_prenda");
        ventana_trabajador.btnBuscarTienda.addActionListener(listener);
        ventana_trabajador.btnBuscarTienda.setActionCommand("Buscar_tienda");
        ventana_trabajador.btnBuscarColTienda.addActionListener(listener);
        ventana_trabajador.btnBuscarColTienda.setActionCommand("Buscar_CT");
        ventana_trabajador.btnBuscarTrabajador.addActionListener(listener);
        ventana_trabajador.btnBuscarTrabajador.setActionCommand("Buscar_trabajador");
        ventana_trabajador.btnBuscarCliente.addActionListener(listener);
        ventana_trabajador.btnBuscarCliente.setActionCommand("Buscar_cliente");
        ventana_trabajador.btnBuscarEncargo.addActionListener(listener);
        ventana_trabajador.btnBuscarEncargo.setActionCommand("Buscar_encargo");
        // BOTONES RECARGAR
        ventana_trabajador.recargarEmpresa.addActionListener(listener);
        ventana_trabajador.recargarEmpresa.setActionCommand("recargarEmpresa");
        ventana_trabajador.recargarDiseniador.addActionListener(listener);
        ventana_trabajador.recargarDiseniador.setActionCommand("recargarDiseniador");
        ventana_trabajador.recargarColeccion.addActionListener(listener);
        ventana_trabajador.recargarColeccion.setActionCommand("recargarColeccion");
        ventana_trabajador.recargarPrenda.addActionListener(listener);
        ventana_trabajador.recargarPrenda.setActionCommand("recargarPrenda");
        ventana_trabajador.recargarTienda.addActionListener(listener);
        ventana_trabajador.recargarTienda.setActionCommand("recargarTienda");
        ventana_trabajador.recargarCT.addActionListener(listener);
        ventana_trabajador.recargarCT.setActionCommand("recargarCT");
        ventana_trabajador.recargarTrabajador.addActionListener(listener);
        ventana_trabajador.recargarTrabajador.setActionCommand("recargarTrabajador");
        ventana_trabajador.recargarCliente.addActionListener(listener);
        ventana_trabajador.recargarCliente.setActionCommand("recargarCliente");
        ventana_trabajador.recargarEncargo.addActionListener(listener);
        ventana_trabajador.recargarEncargo.setActionCommand("recargarEncargo");
        ventana_cliente.recargarCarrito.addActionListener(listener);
        ventana_cliente.recargarCarrito.setActionCommand("recargarCarrito");
        // BOTONES CLIENTE
        ventana_cliente.btnCarrito.addActionListener(listener);
        ventana_cliente.btnCarrito.setActionCommand("Carrito");
        ventana_cliente.btnAnyadirAlCarrito.addActionListener(listener);
        ventana_cliente.btnAnyadirAlCarrito.setActionCommand("Anyadir_alCarrito");
        ventana_cliente.btnFiltrar.addActionListener(listener);
        ventana_cliente.btnFiltrar.setActionCommand("Filtrar");
        // BOTONES CARRITO
        carrito.btnPagar.addActionListener(listener);
        carrito.btnPagar.setActionCommand("Pagar");
        carrito.btnEliminarProducto.addActionListener(listener);
        carrito.btnEliminarProducto.setActionCommand("Eliminar_Producto");
        carrito.btnEliminarTodo.addActionListener(listener);
        carrito.btnEliminarTodo.setActionCommand("Eliminar_todo");
        carrito.salirCarrito.addActionListener(listener);
        // TABLAS
        ventana_trabajador.tablaEmpresa.getSelectionModel().addListSelectionListener(this);
        ventana_trabajador.tablaDisen.getSelectionModel().addListSelectionListener(this);
        ventana_trabajador.tablaColeccion.getSelectionModel().addListSelectionListener(this);
        ventana_trabajador.tablaPrenda.getSelectionModel().addListSelectionListener(this);
        ventana_trabajador.tablaTienda.getSelectionModel().addListSelectionListener(this);
        ventana_trabajador.tablaCT.getSelectionModel().addListSelectionListener(this);
        ventana_trabajador.tablaTrabajador.getSelectionModel().addListSelectionListener(this);
        ventana_trabajador.tablaCliente.getSelectionModel().addListSelectionListener(this);
        ventana_trabajador.tablaEncargo.getSelectionModel().addListSelectionListener(this);
        // INFORMES
        ventana_trabajador.informe_empresas.addActionListener(listener);
        ventana_trabajador.informe_diseniadores.addActionListener(listener);
        ventana_trabajador.informe_colecciones.addActionListener(listener);
        ventana_trabajador.informe_prendas.addActionListener(listener);
        ventana_trabajador.graficoTabla.addActionListener(listener);
        ventana_trabajador.graficoQueso.addActionListener(listener);
        // HTML
        inicio_sesion.manual_usuario.addActionListener(listener);
        inicio_sesion.principal.addActionListener(listener);

    }

    /**
     * Metodo para añadir los window listeners de las ventanas.
     * @param listener
     */
    private void addWindowListeners(WindowListener listener){
        inicio_sesion.addWindowListener(listener);
        ventana_trabajador.addWindowListener(listener);
        ventana_cliente.addWindowListener(listener);
        ventana_administrador.addWindowListener(listener);
    }

    /**
     * Creamos un metodo que añada las columas a un vector para las diferentes listas.
     * @param rs
     * @param columnCount
     * @param data
     * @throws SQLException
     */
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException{
        while (rs.next()){
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++){
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Metodo que llama a los refrescar de cada tabla.
     */
    private void refrescarTodo(){
        refrescarEmpresa();
        refrescarDiseniador();
        refrescarColeccion();
        refrescarPrenda();
        refrescarTienda();
        refrescarTrabajador();
        refrescarCliente();
        refrescarEncargo();
        refrescarCarro();
        refrescarCarrito();
        refrescarCT();
    }

    /**
     * Metodo que llama al metodo setDataVector para construir la tabla para las empresas.
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableEmpresa(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        ventana_trabajador.dtmEmpresa.setDataVector(data, columnNames);
        return ventana_trabajador.dtmEmpresa;
    }

    /**
     * Metodo que refresca las empresas y el combo box de la empresa del diseñador
     */
    private void refrescarEmpresa(){
        try {
            ventana_trabajador.tablaEmpresa.setModel(construirTableEmpresa(modelo.consultarEmpresa()));
            ventana_trabajador.cbEmpresaDisen.removeAllItems();
            for(int i = 0; i< ventana_trabajador.dtmEmpresa.getRowCount(); i++){
                ventana_trabajador.cbEmpresaDisen.addItem(ventana_trabajador.dtmEmpresa.getValueAt(i, 0) + " - " + ventana_trabajador.dtmEmpresa.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que refresca la lista de registros según un parametro que le hayamos dado llamando a otro metodo que conecta con una query en la base de datos.
     */
    private void refrescarEmpresaPorBusqueda(){
        try {
            ventana_trabajador.tablaEmpresa.setModel(construirTableEmpresa(modelo.consultarEmpresaPorBusqueda(ventana_trabajador.txtNombreEmpresa.getText())));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que construye la tabla de los diseñadores
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableDiseniador(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        ventana_trabajador.dtmDiseniador.setDataVector(data, columnNames);
        return ventana_trabajador.dtmDiseniador;
    }

    /**
     * Metodo que refresca los diseñadores y los combobox relacionados con ellos.
     */
    private void refrescarDiseniador(){
        try {
            ventana_trabajador.tablaDisen.setModel(construirTableDiseniador(modelo.consultarDiseniador()));
            ventana_trabajador.cbDiseniadorColeccion.removeAllItems();
            for(int i = 0; i< ventana_trabajador.dtmDiseniador.getRowCount(); i++){
                ventana_trabajador.cbDiseniadorColeccion.addItem(ventana_trabajador.dtmDiseniador.getValueAt(i, 0) + " - " +
                        ventana_trabajador.dtmDiseniador.getValueAt(i, 1) + ", " + ventana_trabajador.dtmDiseniador.getValueAt(i, 2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que refresca los diseñadores según un criterio de busqueda.
     */
    private void refrescarDiseniadorPorBusqueda(){
        try {
            ventana_trabajador.tablaDisen.setModel(construirTableDiseniador(modelo.consultarDiseniadorPorBusqueda(String.valueOf(ventana_trabajador.cbEmpresaDisen.getSelectedItem()))));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que consturye la tabla de colecciones
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableColeccion(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        ventana_trabajador.dtmColeccion.setDataVector(data, columnNames);
        return ventana_trabajador.dtmColeccion;
    }

    /**
     * Metodo que refresca las colecciones y sus combobox relacionados de otras tablas.
     */
    private void refrescarColeccion(){
        try {
            ventana_trabajador.tablaColeccion.setModel(construirTableColeccion(modelo.consultarColeccion()));
            ventana_trabajador.cbColeccionPrenda.removeAllItems();
            ventana_trabajador.cbColeccionCT.removeAllItems();
            for(int i = 0; i< ventana_trabajador.dtmColeccion.getRowCount(); i++){
                ventana_trabajador.cbColeccionPrenda.addItem(ventana_trabajador.dtmColeccion.getValueAt(i, 0) + " - " +
                        ventana_trabajador.dtmColeccion.getValueAt(i, 1));
                ventana_trabajador.cbColeccionCT.addItem(ventana_trabajador.dtmColeccion.getValueAt(i, 0) + " - " +
                        ventana_trabajador.dtmColeccion.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que refresca las colecciones según un criterio.
     */
    private void refrescarColeccionPorBusqueda(){
        try {
            ventana_trabajador.tablaColeccion.setModel(construirTableColeccion(modelo.consultarColeccionPorBusqueda(String.valueOf(ventana_trabajador.cbDiseniadorColeccion.getSelectedItem()))));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que construye la tabla de las prendas.
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTablePrenda(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        ventana_trabajador.dtmPrenda.setDataVector(data, columnNames);
        return ventana_trabajador.dtmPrenda;
    }

    /**
     * Metodo que refresca las prendas.
     */
    private void refrescarPrenda(){
        try {
            ventana_trabajador.tablaPrenda.setModel(construirTablePrenda(modelo.consultarPrenda()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que refresca las prendas según un criterio de busqueda.
     */
    private void refrescarPrendaPorBusqueda(){
        try {
            ventana_trabajador.tablaPrenda.setModel(construirTablePrenda(modelo.consultarPrendaPorBusqueda(String.valueOf(ventana_trabajador.cbColeccionPrenda.getSelectedItem()))));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que construye la tabla de las tiendas.
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableTienda(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        ventana_trabajador.dtmTienda.setDataVector(data, columnNames);
        return ventana_trabajador.dtmTienda;
    }

    /**
     * Metodo que refresca las tiendas y los combobox relacionados de otras tablas.
     */
    private void refrescarTienda(){
        try {
            ventana_trabajador.tablaTienda.setModel(construirTableTienda(modelo.consultarTienda()));
            ventana_trabajador.cbTiendaTrabajador.removeAllItems();
            ventana_trabajador.cbUltimaTienda.removeAllItems();
            ventana_trabajador.cbTiendaCT.removeAllItems();
            for(int i = 0; i< ventana_trabajador.dtmTienda.getRowCount(); i++){
                ventana_trabajador.cbTiendaTrabajador.addItem(ventana_trabajador.dtmTienda.getValueAt(i, 0) + " - " +
                        ventana_trabajador.dtmTienda.getValueAt(i, 1));
                ventana_trabajador.cbUltimaTienda.addItem(ventana_trabajador.dtmTienda.getValueAt(i, 0) + " - " +
                        ventana_trabajador.dtmTienda.getValueAt(i, 1));
                ventana_trabajador.cbTiendaCT.addItem(ventana_trabajador.dtmTienda.getValueAt(i, 0) + " - " +
                        ventana_trabajador.dtmTienda.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que refresca las tiendas según un criterio de busqueda.
     */
    private void refrescarTiendaPorBusqueda(){
        try {
            ventana_trabajador.tablaTienda.setModel(construirTableTienda(modelo.consultarTiendaPorBusqueda(ventana_trabajador.txtNombreTienda.getText())));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que consturye la tabla relacionada entre colecciones y tiendas.
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableCT(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        ventana_trabajador.dtmCT.setDataVector(data, columnNames);
        return ventana_trabajador.dtmCT;
    }

    /**
     * Metodo que refresca la tabla relacionada entre colecciones y tiendas
     */
    private void refrescarCT(){
        try {
            ventana_trabajador.tablaCT.setModel(construirTableCT(modelo.consultarCT()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que refresca según un criterio de busqueda la tabla relacionada entre colecciones y tiendas
     */
    private void refrescarCTPorBusqueda(){
        try {
            ventana_trabajador.tablaCT.setModel(construirTableCT(modelo.consultarCTPorBusqueda(String.valueOf(ventana_trabajador.cbTiendaCT.getSelectedItem()))));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que construye la tabla de trabajadores
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableTrabajador(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        ventana_trabajador.dtmTrabajador.setDataVector(data, columnNames);
        return ventana_trabajador.dtmTrabajador;
    }

    /**
     * Metodo que refresca los trabajadores.
     */
    private void refrescarTrabajador(){
        try {
            ventana_trabajador.tablaTrabajador.setModel(construirTableTrabajador(modelo.consultarTrabajador()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que refresca los trabajadores según un criterio de busqueda
     */
    private void refrescarTrabajadorPorBusqueda(){
        try {
            ventana_trabajador.tablaTrabajador.setModel(construirTableTrabajador(modelo.consultarTrabajadorPorBusqueda(String.valueOf(ventana_trabajador.cbTiendaTrabajador.getSelectedItem()))));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que construye la tabla de clientes
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableCliente(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        ventana_trabajador.dtmCliente.setDataVector(data, columnNames);
        return ventana_trabajador.dtmCliente;
    }

    /**
     * Metodo que refresca los clientes y sus combo box relacionadas de otras tablas.
     */
    private void refrescarCliente(){
        try {
            ventana_trabajador.tablaCliente.setModel(construirTableCliente(modelo.consultarCliente()));
            ventana_trabajador.cbClienteEncargo.removeAllItems();
            for(int i = 0; i< ventana_trabajador.dtmCliente.getRowCount(); i++){
                ventana_trabajador.cbClienteEncargo.addItem(ventana_trabajador.dtmCliente.getValueAt(i, 0) + " - " +
                        ventana_trabajador.dtmCliente.getValueAt(i, 1) + ", " + ventana_trabajador.dtmCliente.getValueAt(i, 2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que refresca los clientes según un criterio de busqueda.
     */
    private void refrescarClientePorBusqueda(){
        try {
            ventana_trabajador.tablaCliente.setModel(construirTableCliente(modelo.consultarClientePorBusqueda(String.valueOf(ventana_trabajador.cbUltimaTienda.getSelectedItem()))));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que construye la tabla de encargos
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableEncargo(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        ventana_trabajador.dtmEncargo.setDataVector(data, columnNames);
        return ventana_trabajador.dtmEncargo;
    }

    /**
     * Metodo que refresca la tabla de encargos con los nuevos registros
     */
    private void refrescarEncargo(){
        try {
            ventana_trabajador.tablaEncargo.setModel(construirTableEncargo(modelo.consultarEncargo()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que refresca los encargos por busqueda.
     */
    private void refrescarEncargoPorBusqueda(){
        try {
            ventana_trabajador.tablaEncargo.setModel(construirTableEncargo(modelo.consultarEncargoPorBusqueda(String.valueOf(ventana_trabajador.cbClienteEncargo.getSelectedItem()))));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTablaCliente(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        ventana_cliente.dtmPrendasCliente.setDataVector(data, columnNames);
        return ventana_cliente.dtmPrendasCliente;
    }

    /**
     * Metodo que refresca el carro con los nuevos registros.
     */
    private void refrescarCarro(){
        try{
            ventana_cliente.tablaCliente.setModel(construirTablaCliente(modelo.consultarPrenda()));
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    /**
     * Metodo que refresca el carro por busqueda.
     */
    private void refrescarCarroPorBusqueda(){
        try {
            ventana_cliente.tablaCliente.setModel(construirTablePrenda(modelo.filtrar(String.valueOf(ventana_cliente.comboFiltro.getSelectedItem()))));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que consturye la tabla del carrito.
     * @param rs
     * @return
     * @throws SQLException
     */
    private DefaultTableModel construirTableCarrito(ResultSet rs) throws SQLException{
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++){
            columnNames.add(metaData.getColumnName(column));
        }
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        carrito.dtmCarrito.setDataVector(data, columnNames);
        return carrito.dtmCarrito;
    }

    /**
     * Metodo que refresca el carrito con los nuevos registros.
     */
    private void refrescarCarrito(){
        try {
            carrito.tableCarrito.setModel(construirTableCarrito(modelo.consultarCarrito()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo switch con el action performed.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            /**
             * Caso que conecta con la base de datos
             */
            case "Conectar":
                modelo.conectar();
                break;
            /**
             * Caso que desconecta con la base de datos
             */
            case "Desconectar":
                modelo.desconectar();
                break;
            /**
             * Caso que inicia sesión a la ventana cliente.
             */
            case "Iniciar_cliente":
                iniciarSesionCliente(inicio_sesion.txtUsuario.getText(),
                        inicio_sesion.txtPass.getText());
                limpiarInicioSesion();
                break;
            /**
             * Caso que inicia sesión a la ventana admin.
             */
            case "Iniciar_admin":
                iniciarSesionAdmin(inicio_sesion.txtUsuario.getText(),
                        inicio_sesion.txtPass.getText());
                limpiarInicioSesion();
                break;
            /**
             * Caso que inicia sesión a la ventana trabajador.
             */
            case "Iniciar_trabajador":
                iniciarSesionTrabajador(inicio_sesion.txtUsuario.getText(),
                        inicio_sesion.txtPass.getText());
                limpiarInicioSesion();
                break;
            /**
             * Caso que crea una cuenta de cliente
             */
            case "Crear_cliente":
                modelo.insertarCuentaCliente(inicio_sesion.txtUsuario.getText(),
                        inicio_sesion.txtPass.getText());
                break;
            /**
             * Caso que crea una cuenta de trabajador
             */
            case "Crear_trabajador":
                modelo.insertarCuentaTrabajador(ventana_administrador.txtUsuarioTrabajador.getText(),
                        ventana_administrador.txtPassTrabajador.getText());
                break;
            /**
             * Caso que cierra sesión del administrador
             */
            case "Cerrar_sesion_admin":
                ventana_administrador.setVisible(false);
                inicio_sesion.setVisible(true);
                break;
            /**
             * Caso que cierra sesión del cliente
             */
            case "Cerrar_sesion_cliente":
                ventana_cliente.setVisible(false);
                inicio_sesion.setVisible(true);
                break;
            /**
             * Caso que cierra sesión del trabajador
             */
            case "Cerrar_sesion_trabajador":
                ventana_trabajador.setVisible(false);
                inicio_sesion.setVisible(true);
                break;
            /**
             * Caso que añade una empresa a la base de datos y a la tabla de empresa
             */
            case "Anyadir_empresa":
                if (modelo.nombreEmpresaYaExiste(ventana_trabajador.txtNombreEmpresa.getText())) {
                    Util.errorAlerta("Esta empresa ya existe");
                    ventana_trabajador.tablaEmpresa.clearSelection();
                } else if (comprobarEmpresaVacia()) {
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaEmpresa.clearSelection();
                } else {
                    modelo.insertarEmpresa(ventana_trabajador.txtNombreEmpresa.getText(),
                            ventana_trabajador.txtSedeEmpresa.getText(),
                            Integer.parseInt(ventana_trabajador.txtCantidadTiendasEmpresa.getText()));
                }
                borrarCamposEmpresa();
                refrescarEmpresa();
                break;
            /**
             * Caso que modifica una empresa de la base de datos y de la tabla de empresa
             */
            case "Modificar_empresa":
                if (comprobarEmpresaVacia()) {
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaEmpresa.clearSelection();
                } else {
                    modelo.modificarEmpresa(ventana_trabajador.txtNombreEmpresa.getText(),
                            ventana_trabajador.txtSedeEmpresa.getText(),
                            Integer.parseInt(ventana_trabajador.txtCantidadTiendasEmpresa.getText()),
                            Integer.parseInt((String) ventana_trabajador.tablaEmpresa.getValueAt(ventana_trabajador.tablaEmpresa.getSelectedRow(), 0)));
                }
                borrarCamposEmpresa();
                refrescarEmpresa();
                break;
            /**
             * Caso que elimina una empresa de la base de datos y de la tabla de empresa
             */
            case "Eliminar_empresa":
                if(ventana_trabajador.tablaEmpresa.getSelectedRow() != -1) {
                    modelo.eliminarEmpresa(Integer.parseInt((String) ventana_trabajador.tablaEmpresa.getValueAt(ventana_trabajador.tablaEmpresa.getSelectedRow(), 0)));
                    borrarCamposEmpresa();
                    refrescarEmpresa();
                }
                break;
            /**
             * Caso que busca registros y los muestra en la tabla de empresa
             */
            case "Buscar_empresa":
                refrescarEmpresaPorBusqueda();
                borrarCamposEmpresa();
                break;
            /**
             * Caso que añade un diseñador a la base de datos y a la tabla de diseñador
             */
            case "Anyadir_disen":
                if (modelo.dniDiseniadorYaExiste(ventana_trabajador.txtDniDiseniador.getText())) {
                    Util.errorAlerta("El dni del diseniador ya existe");
                    ventana_trabajador.tablaDisen.clearSelection();
                }
                else if (ventana_trabajador.txtDniDiseniador.getText().length() < 9){
                    Util.avisoAlerta("El dni no tiene los caracteres minimos");
                }
                else if (comprobarDisenVacio()) {
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaDisen.clearSelection();
                } else {
                    modelo.insertarDiseniador(ventana_trabajador.txtNombreDisen.getText(),
                            ventana_trabajador.txtApeDisen.getText(),
                            Date.valueOf(ventana_trabajador.dateFechaNacDisen.getDate()),
                            ventana_trabajador.txtDniDiseniador.getText(),
                            ventana_trabajador.txtDireccionDisen.getText(),
                            String.valueOf(ventana_trabajador.cbGeneroDisen.getSelectedItem()),
                            ventana_trabajador.txtPaisDisen.getText(),
                            String.valueOf(ventana_trabajador.cbEmpresaDisen.getSelectedItem())
                    );
                }
                borrarCamposDisen();
                refrescarDiseniador();
                break;
            /**
             * Caso que modifica un diseñador de la base de datos y de la tabla de diseñador
             */
            case "Modificar_disen":
                if (comprobarDisenVacio()) {
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaDisen.clearSelection();
                } else if (ventana_trabajador.txtDniDiseniador.getText().length() < 9 && ventana_trabajador.txtDniDiseniador.getText().length() > 9){
                    Util.avisoAlerta("El dni no tiene los caracteres minimos");
                } else {
                    modelo.modificarDiseniador(ventana_trabajador.txtNombreDisen.getText(),
                            ventana_trabajador.txtApeDisen.getText(),
                            Date.valueOf(ventana_trabajador.dateFechaNacDisen.getDate()),
                            ventana_trabajador.txtDniDiseniador.getText(),
                            ventana_trabajador.txtDireccionDisen.getText(),
                            String.valueOf(ventana_trabajador.cbGeneroDisen.getSelectedItem()),
                            ventana_trabajador.txtPaisDisen.getText(),
                            String.valueOf(ventana_trabajador.cbEmpresaDisen.getSelectedItem()),
                            Integer.parseInt((String) ventana_trabajador.tablaDisen.getValueAt(ventana_trabajador.tablaDisen.getSelectedRow(), 0))
                    );
                }
                borrarCamposDisen();
                refrescarDiseniador();
                break;
            /**
             * Caso que elimina un diseñador de la base de datos y de la tabla de diseñador
             */
            case "Eliminar_disen":
                if(ventana_trabajador.tablaDisen.getSelectedRow() != -1) {
                    modelo.eliminarDiseniador(Integer.parseInt((String) ventana_trabajador.tablaDisen.getValueAt(ventana_trabajador.tablaDisen.getSelectedRow(), 0)));
                    borrarCamposDisen();
                    refrescarDiseniador();
                }
                break;
            /**
             * Caso que busca un diseñador y lo muestra
             */
            case "Buscar_disen":
                refrescarDiseniadorPorBusqueda();
                borrarCamposDisen();
                break;
            /**
             * Caso que añade una coleccion a la base de datos y a la tabla de coleccion
             */
            case "Anyadir_coleccion":
                if (modelo.nombreColeccionYaExiste(ventana_trabajador.txtNombreColeccion.getText())) {
                    Util.errorAlerta("La colección ya existe");
                    ventana_trabajador.tablaColeccion.clearSelection();
                } else if(comprobarColeccionVacia()){
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaColeccion.clearSelection();
                }
                else {
                    modelo.insertarColeccion(ventana_trabajador.txtNombreColeccion.getText(),
                            String.valueOf(ventana_trabajador.cbMarcaColeccion.getSelectedItem()),
                            String.valueOf(ventana_trabajador.cbEstacionColeccion.getSelectedItem()),
                            Date.valueOf(ventana_trabajador.dateCreacionColeccion.getDate()),
                            String.valueOf(ventana_trabajador.cbDiseniadorColeccion.getSelectedItem()));
                }
                borrarCamposColeccion();
                refrescarColeccion();
                break;
            /**
             * Caso que modifica una coleccion de la base de datos y de la tabla de coleccion
             */
            case "Modificar_coleccion":
                if(comprobarColeccionVacia()){
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaColeccion.clearSelection();
                } else {
                    modelo.modificarColeccion(ventana_trabajador.txtNombreColeccion.getText(),
                            String.valueOf(ventana_trabajador.cbMarcaColeccion.getSelectedItem()),
                            String.valueOf(ventana_trabajador.cbEstacionColeccion.getSelectedItem()),
                            Date.valueOf(ventana_trabajador.dateCreacionColeccion.getDate()),
                            String.valueOf(ventana_trabajador.cbDiseniadorColeccion.getSelectedItem()),
                            Integer.parseInt((String) ventana_trabajador.tablaColeccion.getValueAt(ventana_trabajador.tablaColeccion.getSelectedRow(), 0)));
                }
                borrarCamposColeccion();
                refrescarColeccion();
                break;
            /**
             * Caso que elimina una coleccion de la base de datos y de la tabla de coleccion
             */
            case "Eliminar_coleccion":
                if(ventana_trabajador.tablaColeccion.getSelectedRow() != -1) {
                    modelo.eliminarColeccion(Integer.parseInt((String) ventana_trabajador.tablaColeccion.getValueAt(ventana_trabajador.tablaColeccion.getSelectedRow(), 0)));
                    borrarCamposColeccion();
                    refrescarColeccion();
                }
                break;
            /**
             * Caso que busca una colección y la muestra
             */
            case "Buscar_coleccion":
                refrescarColeccionPorBusqueda();
                break;
            /**
             * Caso que añade una prenda a la base de datos y a la tabla de prenda
             */
            case "Anyadir_prenda":
                if (modelo.nombrePrendaYaExiste(ventana_trabajador.txtNombrePrenda.getText())) {
                    Util.errorAlerta("Esta prenda ya existe");
                    ventana_trabajador.tablaPrenda.clearSelection();
                } else if (comprobarPrendaVacia()) {
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaPrenda.clearSelection();
                } else {
                    if (ventana_trabajador.mujerRadioButton.isSelected()) {
                        modelo.insertarPrenda(ventana_trabajador.txtNombrePrenda.getText(),
                                ventana_trabajador.mujerRadioButton.getText(),
                                String.valueOf(ventana_trabajador.cbTipoPrenda.getSelectedItem()),
                                String.valueOf(ventana_trabajador.cbColorPrenda.getSelectedItem()),
                                ventana_trabajador.sliderTallaPrenda.getValue(),
                                Float.valueOf(ventana_trabajador.txtPrecioPrenda.getText()),
                                String.valueOf(ventana_trabajador.cbColeccionPrenda.getSelectedItem()));
                    } else if (ventana_trabajador.hombreRadioButton.isSelected()) {
                        modelo.insertarPrenda(ventana_trabajador.txtNombrePrenda.getText(),
                                ventana_trabajador.hombreRadioButton.getText(),
                                String.valueOf(ventana_trabajador.cbTipoPrenda.getSelectedItem()),
                                String.valueOf(ventana_trabajador.cbColorPrenda.getSelectedItem()),
                                ventana_trabajador.sliderTallaPrenda.getValue(),
                                Float.valueOf(ventana_trabajador.txtPrecioPrenda.getText()),
                                String.valueOf(ventana_trabajador.cbColeccionPrenda.getSelectedItem()));
                    } else if (ventana_trabajador.niniaRadioButton.isSelected()) {
                        modelo.insertarPrenda(ventana_trabajador.txtNombrePrenda.getText(),
                                ventana_trabajador.niniaRadioButton.getText(),
                                String.valueOf(ventana_trabajador.cbTipoPrenda.getSelectedItem()),
                                String.valueOf(ventana_trabajador.cbColorPrenda.getSelectedItem()),
                                ventana_trabajador.sliderTallaPrenda.getValue(),
                                Float.valueOf(ventana_trabajador.txtPrecioPrenda.getText()),
                                String.valueOf(ventana_trabajador.cbColeccionPrenda.getSelectedItem()));
                    } else if (ventana_trabajador.ninioRadioButton.isSelected()) {
                        modelo.insertarPrenda(ventana_trabajador.txtNombrePrenda.getText(),
                                ventana_trabajador.ninioRadioButton.getText(),
                                String.valueOf(ventana_trabajador.cbTipoPrenda.getSelectedItem()),
                                String.valueOf(ventana_trabajador.cbColorPrenda.getSelectedItem()),
                                ventana_trabajador.sliderTallaPrenda.getValue(),
                                Float.valueOf(ventana_trabajador.txtPrecioPrenda.getText()),
                                String.valueOf(ventana_trabajador.cbColeccionPrenda.getSelectedItem()));
                    }
                }
                borrarCamposPrenda();
                refrescarPrenda();
                break;
            /**
             * Caso que modifica una prenda de la base de datos y de la tabla de prenda
             */
            case "Modificar_prenda":
                if (comprobarPrendaVacia()) {
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaPrenda.clearSelection();
                } else {
                    if (ventana_trabajador.mujerRadioButton.isSelected()) {
                        modelo.modificarPrenda(ventana_trabajador.txtNombrePrenda.getText(),
                                ventana_trabajador.mujerRadioButton.getText(),
                                String.valueOf(ventana_trabajador.cbTipoPrenda.getSelectedItem()),
                                String.valueOf(ventana_trabajador.cbColorPrenda.getSelectedItem()),
                                ventana_trabajador.sliderTallaPrenda.getValue(),
                                Float.valueOf(ventana_trabajador.txtPrecioPrenda.getText()),
                                String.valueOf(ventana_trabajador.cbColeccionPrenda.getSelectedItem()),
                                Integer.parseInt((String) ventana_trabajador.tablaPrenda.getValueAt(ventana_trabajador.tablaPrenda.getSelectedRow(), 0))
                        );
                    } else if (ventana_trabajador.hombreRadioButton.isSelected()) {
                        modelo.modificarPrenda(ventana_trabajador.txtNombrePrenda.getText(),
                                ventana_trabajador.hombreRadioButton.getText(),
                                String.valueOf(ventana_trabajador.cbTipoPrenda.getSelectedItem()),
                                String.valueOf(ventana_trabajador.cbColorPrenda.getSelectedItem()),
                                ventana_trabajador.sliderTallaPrenda.getValue(),
                                Float.valueOf(ventana_trabajador.txtPrecioPrenda.getText()),
                                String.valueOf(ventana_trabajador.cbColeccionPrenda.getSelectedItem()),
                                Integer.parseInt((String) ventana_trabajador.tablaPrenda.getValueAt(ventana_trabajador.tablaPrenda.getSelectedRow(), 0))
                        );
                    } else if (ventana_trabajador.niniaRadioButton.isSelected()) {
                        modelo.modificarPrenda(ventana_trabajador.txtNombrePrenda.getText(),
                                ventana_trabajador.niniaRadioButton.getText(),
                                String.valueOf(ventana_trabajador.cbTipoPrenda.getSelectedItem()),
                                String.valueOf(ventana_trabajador.cbColorPrenda.getSelectedItem()),
                                ventana_trabajador.sliderTallaPrenda.getValue(),
                                Float.valueOf(ventana_trabajador.txtPrecioPrenda.getText()),
                                String.valueOf(ventana_trabajador.cbColeccionPrenda.getSelectedItem()),
                                Integer.parseInt((String) ventana_trabajador.tablaPrenda.getValueAt(ventana_trabajador.tablaPrenda.getSelectedRow(), 0))
                        );
                    } else if (ventana_trabajador.ninioRadioButton.isSelected()) {
                        modelo.modificarPrenda(ventana_trabajador.txtNombrePrenda.getText(),
                                ventana_trabajador.ninioRadioButton.getText(),
                                String.valueOf(ventana_trabajador.cbTipoPrenda.getSelectedItem()),
                                String.valueOf(ventana_trabajador.cbColorPrenda.getSelectedItem()),
                                ventana_trabajador.sliderTallaPrenda.getValue(),
                                Float.valueOf(ventana_trabajador.txtPrecioPrenda.getText()),
                                String.valueOf(ventana_trabajador.cbColeccionPrenda.getSelectedItem()),
                                Integer.parseInt((String) ventana_trabajador.tablaPrenda.getValueAt(ventana_trabajador.tablaPrenda.getSelectedRow(), 0))
                        );
                    }
                }
                borrarCamposPrenda();
                refrescarPrenda();
                break;
            /**
             * Caso que elimina una prenda de la base de datos y de la tabla de prenda
             */
            case "Eliminar_prenda":
                if(ventana_trabajador.tablaPrenda.getSelectedRow() != -1) {
                    modelo.eliminarPrenda(Integer.parseInt((String) ventana_trabajador.tablaPrenda.getValueAt(ventana_trabajador.tablaPrenda.getSelectedRow(), 0)));
                    borrarCamposPrenda();
                    refrescarPrenda();
                }
                break;
            /**
             * Caso que busca una prenda y la muestra
             */
            case "Buscar_prenda":
                refrescarPrendaPorBusqueda();
                break;
            /**
             * Caso que añade una tienda a la base de datos y a la tabla de tienda
             */
            case "Anyadir_tienda":
                if (modelo.nombreTiendaYaExiste(ventana_trabajador.txtNombreTienda.getText())) {
                    Util.errorAlerta("La tienda ya existe");
                    ventana_trabajador.tablaTienda.clearSelection();
                } else if(comprobarTiendaVacia()){
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaTienda.clearSelection();
                } else {
                    modelo.insertarTienda(ventana_trabajador.txtNombreTienda.getText(),
                            ventana_trabajador.txtDireccionTienda.getText(),
                            ventana_trabajador.txtCiudadTienda.getText(),
                            ventana_trabajador.sliderPlantasTienda.getValue()
                    );
                }
                borrarCamposTienda();
                refrescarTienda();
                break;
            /**
             * Caso que modifica una tienda de la base de datos y de la tabla de tienda
             */
            case "Modificar_tienda":
                if(comprobarTiendaVacia()){
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaTienda.clearSelection();
                } else {
                    modelo.modificarTienda(ventana_trabajador.txtNombreTienda.getText(),
                            ventana_trabajador.txtDireccionTienda.getText(),
                            ventana_trabajador.txtCiudadTienda.getText(),
                            ventana_trabajador.sliderPlantasTienda.getValue(),
                            Integer.parseInt((String) ventana_trabajador.tablaTienda.getValueAt(ventana_trabajador.tablaTienda.getSelectedRow(), 0))
                            );
                }
                borrarCamposTienda();
                refrescarTienda();
                break;
            /**
             * Caso que elimina una tienda de la base de datos y de la tabla de tienda
             */
            case "Eliminar_tienda":
                if(ventana_trabajador.tablaTienda.getSelectedRow() != -1) {
                    modelo.eliminarTienda(Integer.parseInt((String) ventana_trabajador.tablaTienda.getValueAt(ventana_trabajador.tablaTienda.getSelectedRow(), 0)));
                    borrarCamposTienda();
                    refrescarTienda();
                }
                break;
            /**
             * Caso que busca una tienda y la muestra
             */
            case "Buscar_tienda":
                refrescarTiendaPorBusqueda();
                break;
            /**
             * Caso que añade un registro a la tabla relacional de colección y tienda a la base de datos y a la tabla de relacionada
             */
            case "Anyadir_CT":
                modelo.insertarCT(String.valueOf(ventana_trabajador.cbColeccionCT.getSelectedItem()),
                        String.valueOf(ventana_trabajador.cbTiendaCT.getSelectedItem()));
                borrarCamposCT();
                refrescarCT();
                break;
            /**
             * Caso que modifica un registro de la tabla relacional de colección y tienda de la base de datos y de la tabla de relacionada
             */
            case "Modificar_CT":
                modelo.modificarCT(String.valueOf(ventana_trabajador.cbColeccionCT.getSelectedItem()),
                        String.valueOf(ventana_trabajador.cbTiendaCT.getSelectedItem()),
                        Integer.parseInt((String) ventana_trabajador.tablaCT.getValueAt(ventana_trabajador.tablaCT.getSelectedRow(), 0))
                );

                borrarCamposCT();
                refrescarCT();
                break;
            /**
             * Caso que elimina un registro de la tabla relacional de colección y tienda de la base de datos y de la tabla de relacionada
             */
            case "Eliminar_CT":
                if(ventana_trabajador.tablaCT.getSelectedRow() != -1) {
                    modelo.eliminarCT(Integer.parseInt((String) ventana_trabajador.tablaCT.getValueAt(ventana_trabajador.tablaCT.getSelectedRow(), 0)));
                    borrarCamposCT();
                    refrescarCT();
                }
                break;
            /**
             * Caso que busca un registro de la tabla relacionada de colección y tienda y lo muestra
             */
            case "Buscar_CT":
                refrescarCTPorBusqueda();
                break;
            /**
             * Caso que añade un registro de la tabla trabajador a la base de datos y a la tabla de relacionada
             */
            case "Anyadir_trabajador":
                if (modelo.dniTrabajadorYaExiste(ventana_trabajador.txtDniTrabajador.getText())) {
                    Util.errorAlerta("El dni del trabajador ya existe");
                    ventana_trabajador.tablaTrabajador.clearSelection();
                } else if (ventana_trabajador.txtDniTrabajador.getText().length() < 9 && ventana_trabajador.txtDniTrabajador.getText().length() > 9){
                    Util.avisoAlerta("El dni no tiene los caracteres minimos");
                }else if(comprobarTrabajadorVacio()){
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaTrabajador.clearSelection();
                } else {
                    modelo.insertarTrabajador(ventana_trabajador.txtNombreTrabajador.getText(),
                            ventana_trabajador.txtApellidosTrabajador.getText(),
                            Date.valueOf(ventana_trabajador.dateNacimientoTrabajador.getDate()),
                            ventana_trabajador.txtDniTrabajador.getText(),
                            String.valueOf(ventana_trabajador.cbGeneroTrabajador.getSelectedItem()),
                            String.valueOf(ventana_trabajador.cbCargoTrabajador.getSelectedItem()),
                            String.valueOf(ventana_trabajador.cbTiendaTrabajador.getSelectedItem()));
                }
                borrarCamposTrabajador();
                refrescarTrabajador();
                break;
            /**
             * Caso que modifica un registro de la tabla trabajador de la base de datos y de la tabla de relacionada
             */
            case "Modificar_trabajador":
                if(comprobarTrabajadorVacio()){
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaTrabajador.clearSelection();
                } else if (ventana_trabajador.txtDniTrabajador.getText().length() < 9){
                    Util.avisoAlerta("El dni no tiene los caracteres minimos");
                } else {
                    modelo.modificarTrabajador(ventana_trabajador.txtNombreTrabajador.getText(),
                            ventana_trabajador.txtApellidosTrabajador.getText(),
                            Date.valueOf(ventana_trabajador.dateNacimientoTrabajador.getDate()),
                            ventana_trabajador.txtDniTrabajador.getText(),
                            String.valueOf(ventana_trabajador.cbGeneroTrabajador.getSelectedItem()),
                            String.valueOf(ventana_trabajador.cbCargoTrabajador.getSelectedItem()),
                            String.valueOf(ventana_trabajador.cbTiendaTrabajador.getSelectedItem()),
                            Integer.parseInt((String) ventana_trabajador.tablaTrabajador.getValueAt(ventana_trabajador.tablaTrabajador.getSelectedRow(), 0))
                    );
                }
                borrarCamposTrabajador();
                refrescarTrabajador();
                break;
            /**
             * Caso que elimina un registro de la tabla trabajador de la base de datos y de la tabla de relacionada
             */
            case "Eliminar_trabajador":
                if(ventana_trabajador.tablaTrabajador.getSelectedRow() != -1) {
                    modelo.eliminarTrabajador(Integer.parseInt((String) ventana_trabajador.tablaTrabajador.getValueAt(ventana_trabajador.tablaTrabajador.getSelectedRow(), 0)));
                    refrescarTrabajador();
                }
                break;
            /**
             * Caso que busca un trabajador y lo muestra
             */
            case "Buscar_trabajador":
                refrescarTrabajadorPorBusqueda();
                break;
            /**
             * Caso que añade un registro de la tabla cliente a la base de datos y a la tabla de relacionada
             */
            case "Anyadir_cliente":
                if (modelo.dniClienteYaExiste(ventana_trabajador.txtDniCliente.getText())) {
                    Util.errorAlerta("El dni del cliente ya existe");
                    ventana_trabajador.tablaCliente.clearSelection();
                }else if (ventana_trabajador.txtDniCliente.getText().length() < 9 && ventana_trabajador.txtDniCliente.getText().length() > 9){
                    Util.avisoAlerta("El dni no tiene los caracteres minimos");
                } else if(comprobarClienteVacio()){
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaCliente.clearSelection();
                } else {
                    modelo.insertarCliente(ventana_trabajador.txtNombreCliente.getText(),
                            ventana_trabajador.txtApellidosCliente.getText(),
                            Date.valueOf(ventana_trabajador.dateNacimientoCliente.getDate()),
                            ventana_trabajador.txtDniCliente.getText(),
                            String.valueOf(ventana_trabajador.cbGeneroCliente.getSelectedItem()),
                            ventana_trabajador.txtCorreoCliente.getText(),
                            String.valueOf(ventana_trabajador.cbUltimaTienda.getSelectedItem()));
                }
                borrarCamposCliente();
                refrescarCliente();
                break;
            /**
             * Caso que modifica un registro de la tabla cliente de la base de datos y de la tabla de relacionada
             */
            case "Modificar_cliente":
                if(comprobarClienteVacio()){
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaCliente.clearSelection();
                } else if (ventana_trabajador.txtDniCliente.getText().length() < 9){
                    Util.avisoAlerta("El dni no tiene los caracteres minimos");
                } else {
                    modelo.modificarCliente(ventana_trabajador.txtNombreCliente.getText(),
                            ventana_trabajador.txtApellidosCliente.getText(),
                            Date.valueOf(ventana_trabajador.dateNacimientoCliente.getDate()),
                            ventana_trabajador.txtDniCliente.getText(),
                            String.valueOf(ventana_trabajador.cbGeneroCliente.getSelectedItem()),
                            ventana_trabajador.txtCorreoCliente.getText(),
                            String.valueOf(ventana_trabajador.cbUltimaTienda.getSelectedItem()),
                            Integer.parseInt((String) ventana_trabajador.tablaCliente.getValueAt(ventana_trabajador.tablaCliente.getSelectedRow(), 0))
                    );
                }
                borrarCamposCliente();
                refrescarCliente();
                break;
            /**
             * Caso que elimina un registro de la tabla cliente de la base de datos y de la tabla de relacionada
             */
            case "Eliminar_cliente":
                if(ventana_trabajador.tablaCliente.getSelectedRow() != -1) {
                    modelo.eliminarCliente(Integer.parseInt((String) ventana_trabajador.tablaCliente.getValueAt(ventana_trabajador.tablaCliente.getSelectedRow(), 0)));
                    borrarCamposCliente();
                    refrescarCliente();
                }
                break;
            /**
             * Caso que busca un cliente y lo muestra
             */
            case "Buscar_cliente":
                refrescarClientePorBusqueda();
                break;
            /**
             * Caso que añade un registro de la tabla encargo a la base de datos y a la tabla de relacionada
             */
            case "Anyadir_encargo":
                if(comprobarEncargoVacio()){
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaEncargo.clearSelection();
                } else {
                    modelo.insertarEncargo(Integer.parseInt(ventana_trabajador.txtNumeroPrendasEncargo.getText()),
                            Float.parseFloat(ventana_trabajador.txtPrecioTotalEncargo.getText()),
                            Date.valueOf(ventana_trabajador.dateEncargo.getDate()),
                            Date.valueOf(ventana_trabajador.dateEntregaEncargo.getDate()),
                            String.valueOf(ventana_trabajador.cbClienteEncargo.getSelectedItem()));
                }
                borrarCamposEncargo();
                refrescarEncargo();
                break;
            /**
             * Caso que modifica un registro de la tabla encargo de la base de datos y de la tabla de relacionada
             */
            case "Modificar_encargo":
                if(comprobarEncargoVacio()){
                    Util.errorAlerta("Rellena todos los campos");
                    ventana_trabajador.tablaEncargo.clearSelection();
                } else {
                    modelo.modificarEncargo(Integer.parseInt(ventana_trabajador.txtNumeroPrendasEncargo.getText()),
                            Float.parseFloat(ventana_trabajador.txtPrecioTotalEncargo.getText()),
                            Date.valueOf(ventana_trabajador.dateEncargo.getDate()),
                            Date.valueOf(ventana_trabajador.dateEntregaEncargo.getDate()),
                            String.valueOf(ventana_trabajador.cbClienteEncargo.getSelectedItem()),
                            Integer.parseInt((String) ventana_trabajador.tablaEncargo.getValueAt(ventana_trabajador.tablaEncargo.getSelectedRow(), 0))
                            );
                }
                borrarCamposEncargo();
                refrescarEncargo();
                break;
            /**
             * Caso que elimina un registro de la tabla encargo de la base de datos y de la tabla de relacionada
             */
            case "Eliminar_encargo":
                if(ventana_trabajador.tablaEncargo.getSelectedRow() != -1) {
                    modelo.eliminarEncargo(Integer.parseInt((String) ventana_trabajador.tablaEncargo.getValueAt(ventana_trabajador.tablaEncargo.getSelectedRow(), 0)));
                    borrarCamposEncargo();
                    refrescarEncargo();
                }
                break;
            /**
             * Caso que busca un encargo y lo muestra
             */
            case "Buscar_encargo":
                refrescarEncargoPorBusqueda();
                break;
            /**
             * Caso que filtra las prendas de la ventana cliente
             */
            case "Filtrar":
                refrescarCarroPorBusqueda();
                /**
                 * Caso que añade registros seleccionados de la tabla de la ventana del cliente y las añade al carrito.
                 */
            case "Anyadir_alCarrito":
                if(ventana_cliente.tablaCliente.getSelectedRow() != -1) {
                    modelo.insertarPrendaCarrito(String.valueOf(ventana_cliente.tablaCliente.getValueAt(ventana_cliente.tablaCliente.getSelectedRow(), 1)),
                            Integer.parseInt((String) ventana_cliente.tablaCliente.getValueAt(ventana_cliente.tablaCliente.getSelectedRow(), 5)),
                            String.valueOf(ventana_cliente.tablaCliente.getValueAt(ventana_cliente.tablaCliente.getSelectedRow(), 4)),
                            String.valueOf(ventana_cliente.tablaCliente.getValueAt(ventana_cliente.tablaCliente.getSelectedRow(), 3)),
                            Float.parseFloat(String.valueOf(ventana_cliente.tablaCliente.getValueAt(ventana_cliente.tablaCliente.getSelectedRow(), 6))));
                    precioTotal = Float.parseFloat(String.valueOf(ventana_cliente.tablaCliente.getValueAt(ventana_cliente.tablaCliente.getSelectedRow(), 6))) + precioTotal;
                    carrito.labelPrecio.setText("Precio total: " + precioTotal + " €");
                    refrescarCarrito();
                }
                break;
                /**
                 * Caso que abre el carrito
                 */
            case "Carrito":
                ventana_cliente.btnCarrito.setEnabled(false);
                carrito.setVisible(true);
                break;
            /**
             *  Caso que te deja salir de carrito y cerrar esa ventana
             */
            case "Salir_carrito":
                carrito.setVisible(false);
                ventana_cliente.btnCarrito.setEnabled(true);
                break;
            /**
             * Caso que te muestra un mensaje al pagar y elimina los datos del carrito
             */
            case "Pagar":
                pago.setVisible(true);
                modelo.eliminarCarrito();
                break;
            /**
             * Caso que elimina un producto del carrito
             */
            case "Eliminar_Producto":
                modelo.eliminarPrendaCarrito(String.valueOf(carrito.tableCarrito.getValueAt(carrito.tableCarrito.getSelectedRow(), 0)));
                precioTotal = precioTotal - Float.parseFloat(String.valueOf(carrito.tableCarrito.getValueAt(carrito.tableCarrito.getSelectedRow(), 4)));
                carrito.labelPrecio.setText("Precio total: " + precioTotal + " €");
                refrescarCarrito();
                break;
            /**
             * Caso que elimina todos los registros del carrito
             */
            case "Eliminar_todo":
                modelo.eliminarCarrito();
                precioTotal = 0;
                carrito.labelPrecio.setText("Precio total: " + precioTotal + " €");
                refrescarCarrito();
                break;
            /**
             * Caso que abre el informe de empresa
             */
            case "Informe_empresas":
                Informes.informe("Informe_empresa.jasper", "Informe_empresa.pdf");
                break;
            /**
             * Caso que abre el informe de diseñador
             */
            case "Informe_diseniadores":
                Informes.informe("Informe_disen.jasper", "Informe_disen.pdf");
                break;
            /**
             * Caso que abre el informe de coleccion
             */
            case "Informe_colecciones":
                Informes.informe("Informe_coleccion.jasper", "Informe_coleccion.pdf");
                break;
            /**
             * Caso que abre el informe de prenda
             */
            case "Informe_prendas":
                Informes.informe("Informe_prendas.jasper", "Informe_prendas.pdf");
                break;
            /**
             * Caso que abre el la tabla
             */
            case "Tabla":
                Informes.informe("Tabla.jasper", "Tabla.pdf");
                break;
            /**
             * Caso que abre el grafico
             */
            case "Redondo":
                Informes.informe("Redondo.jasper", "Redondo.pdf");
                break;
            case "Principal":
                inicio_sesion.ponLaAyuda();
                break;
            case "Manual":
                inicio_sesion.ponLaAyuda();
                break;
            case "recargarEmpresa":
                refrescarEmpresa();
                break;
            case "recargarDiseniador":
                refrescarDiseniador();
                break;
            case "recargarColeccion":
                refrescarColeccion();
                break;
            case "recargarPrenda":
                refrescarPrenda();
                break;
            case "recargarTienda":
                refrescarTienda();
                break;
            case "recargarCT":
                refrescarCT();
                break;
            case "recargarCliente":
                refrescarCliente();
                break;
            case "recargarTrabajador":
                refrescarTrabajador();
                break;
            case "recargarEncargo":
                refrescarEncargo();
                break;
            case "recargarCarrito":
                refrescarCarro();
                break;
        }
    }

    /**
     * Meotodo que de una query agarra el usuario y la contraseña y si estan inicia sesión
     * @param usuario
     * @param contrasenia
     */
    void iniciarSesionCliente(String usuario, String contrasenia) {
        int resultado = 0;
        String sentenciaSql = "SELECT usuario, contrasenia FROM login WHERE usuario=(?) " +
                "AND contrasenia=(?) AND tipo_cuenta='Cliente'";
        try {
            PreparedStatement sentencia = modelo.conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, usuario);
            sentencia.setString(2, contrasenia);
            ResultSet rs = sentencia.executeQuery();

            if (rs.next()) {
                resultado = 1;
                if (resultado == 1) {
                    inicio_sesion.dispose();
                    ventana_cliente.setVisible(true);
                    resultado = 0;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Metodo que agarra la contraseña y el usuario y si estan dentro de la base de datos con el tipo de cuenta trabajador entonces inicia sesión
     * @param usuario
     * @param contrasenia
     */
    void iniciarSesionTrabajador(String usuario, String contrasenia) {
        int resultado = 0;
        String sentenciaSql = "SELECT usuario, contrasenia FROM login WHERE usuario=(?) " +
                "AND contrasenia=(?) AND tipo_cuenta='Trabajador'";
        try {
            PreparedStatement sentencia = modelo.conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, usuario);
            sentencia.setString(2, contrasenia);
            ResultSet rs = sentencia.executeQuery();

            if (rs.next()) {
                resultado = 1;
                if (resultado == 1) {
                    inicio_sesion.dispose();
                    ventana_trabajador.setVisible(true);
                    resultado = 0;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Metodo que inicia sesión en el admin si los datos estan en la base de datos
     * @param usuario
     * @param contrasenia
     */
    void iniciarSesionAdmin(String usuario, String contrasenia) {
        int resultado = 0;
        String sentenciaSql = "SELECT usuario, contrasenia FROM login WHERE usuario=(?) " +
                "AND contrasenia=(?) AND tipo_cuenta='Administrador'";
        try {
            PreparedStatement sentencia = modelo.conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, usuario);
            sentencia.setString(2, contrasenia);
            ResultSet rs = sentencia.executeQuery();

            if (rs.next()) {
                resultado = 1;
                if (resultado == 1) {
                    inicio_sesion.dispose();
                    ventana_administrador.setVisible(true);
                    resultado = 0;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Metoodo que limpia los campos del inicio de sesión
     */
    void limpiarInicioSesion() {
        inicio_sesion.txtUsuario.setText("");
        inicio_sesion.txtPass.setText("");
    }

    /**
     * Metodo que borra los campos de la empresa
     */
    private void borrarCamposEmpresa(){
        ventana_trabajador.txtNombreEmpresa.setText("");
        ventana_trabajador.txtSedeEmpresa.setText("");
        ventana_trabajador.txtCantidadTiendasEmpresa.setText("");
    }

    /**
     * Metodo que comprueba que algún campo esta vacio en la empresa
     * @return
     */
    private boolean comprobarEmpresaVacia(){
        return ventana_trabajador.txtNombreEmpresa.getText().isEmpty() ||
                ventana_trabajador.txtSedeEmpresa.getText().isEmpty() ||
                ventana_trabajador.txtCantidadTiendasEmpresa.getText().isEmpty();
    }
    /**
     * Metodo que borra los campos del diseñador
     */
    private void borrarCamposDisen(){
        ventana_trabajador.txtNombreDisen.setText("");
        ventana_trabajador.txtApeDisen.setText("");
        ventana_trabajador.dateFechaNacDisen.setText("");
        ventana_trabajador.txtDniDiseniador.setText("");
        ventana_trabajador.txtDireccionDisen.setText("");
        ventana_trabajador.cbGeneroDisen.setSelectedItem(-1);
        ventana_trabajador.txtPaisDisen.setText("");
        ventana_trabajador.cbEmpresaDisen.setSelectedItem(-1);
    }
    /**
     * Metodo que comprueba que algún campo esta vacio en la empresa
     * @return
     */
    private boolean comprobarDisenVacio(){
        return ventana_trabajador.txtNombreDisen.getText().isEmpty() ||
                ventana_trabajador.txtApeDisen.getText().isEmpty() ||
                ventana_trabajador.dateFechaNacDisen.getText().isEmpty() ||
                ventana_trabajador.txtDniDiseniador.getText().isEmpty() ||
                ventana_trabajador.txtDireccionDisen.getText().isEmpty() ||
                ventana_trabajador.cbGeneroDisen.getSelectedIndex() == -1 ||
                ventana_trabajador.txtPaisDisen.getText().isEmpty() ||
                ventana_trabajador.cbEmpresaDisen.getSelectedIndex() == -1;
    }
    /**
     * Metodo que borra los campos de la coleccion
     */
    private void borrarCamposColeccion(){
        ventana_trabajador.txtNombreColeccion.setText("");
        ventana_trabajador.cbMarcaColeccion.setSelectedItem(-1);
        ventana_trabajador.cbEstacionColeccion.setSelectedItem(-1);
        ventana_trabajador.dateCreacionColeccion.setText("");
        ventana_trabajador.cbDiseniadorColeccion.setSelectedItem(-1);
    }
    /**
     * Metodo que comprueba que algún campo esta vacio en la coleccion
     * @return
     */
    private boolean comprobarColeccionVacia(){
        return ventana_trabajador.txtNombreColeccion.getText().isEmpty() ||
                ventana_trabajador.cbMarcaColeccion.getSelectedIndex() == -1 ||
                ventana_trabajador.cbEstacionColeccion.getSelectedIndex() == -1 ||
                ventana_trabajador.dateCreacionColeccion.getText().isEmpty() ||
                ventana_trabajador.cbDiseniadorColeccion.getSelectedIndex() == -1;
    }
    /**
     * Metodo que borra los campos de la prenda
     */
    private void borrarCamposPrenda(){
        ventana_trabajador.txtNombrePrenda.setText("");
        ventana_trabajador.cbColorPrenda.setSelectedItem(-1);
        ventana_trabajador.cbTipoPrenda.setSelectedItem(-1);
        ventana_trabajador.sliderTallaPrenda.setValue(38);
        ventana_trabajador.txtPrecioPrenda.setText("");
        ventana_trabajador.cbColeccionPrenda.setSelectedItem(-1);
    }
    /**
     * Metodo que comprueba que algún campo esta vacio de la prenda
     * @return
     */
    private boolean comprobarPrendaVacia(){
        return ventana_trabajador.txtNombrePrenda.getText().isEmpty() ||
                ventana_trabajador.cbColorPrenda.getSelectedIndex() == -1 ||
                ventana_trabajador.cbTipoPrenda.getSelectedIndex() == -1 ||
                ventana_trabajador.txtPrecioPrenda.getText().isEmpty() ||
                ventana_trabajador.cbColeccionPrenda.getSelectedIndex() == -1;
    }
    /**
     * Metodo que borra los campos de la tienda
     */
    private void borrarCamposTienda(){
        ventana_trabajador.txtNombreTienda.setText("");
        ventana_trabajador.txtDireccionTienda.setText("");
        ventana_trabajador.txtCiudadTienda.setText("");
        ventana_trabajador.sliderPlantasTienda.setValue(3);
    }
    /**
     * Metodo que comprueba que algún campo esta vacio de la tienda
     * @return
     */
    private boolean comprobarTiendaVacia(){
        return ventana_trabajador.txtNombreTienda.getText().isEmpty() ||
                ventana_trabajador.txtDireccionTienda.getText().isEmpty() ||
                ventana_trabajador.txtCiudadTienda.getText().isEmpty();
    }
    /**
     * Metodo que borra los campos de la tabla relacionada de colecciones y tienda
     */
    private void borrarCamposCT(){
        ventana_trabajador.cbColeccionCT.setSelectedItem(-1);
        ventana_trabajador.cbTiendaCT.setSelectedItem(-1);
    }
    /**
     * Metodo que comprueba que algún campo esta vacio en el trabajador
     * @return
     */
    private void borrarCamposTrabajador(){
        ventana_trabajador.txtNombreTrabajador.setText("");
        ventana_trabajador.txtApellidosTrabajador.setText("");
        ventana_trabajador.cbGeneroTrabajador.setSelectedItem(-1);
        ventana_trabajador.dateNacimientoTrabajador.setText("");
        ventana_trabajador.txtDniTrabajador.setText("");
        ventana_trabajador.cbCargoTrabajador.setSelectedItem(-1);
        ventana_trabajador.cbTiendaTrabajador.setSelectedItem(-1);
    }
    /**
     * Metodo que comprueba que algún campo esta vacio en el trabajador
     * @return
     */
    private boolean comprobarTrabajadorVacio(){
        return ventana_trabajador.txtNombreTrabajador.getText().isEmpty() ||
                ventana_trabajador.txtApellidosTrabajador.getText().isEmpty() ||
                ventana_trabajador.cbGeneroTrabajador.getSelectedIndex() == -1 ||
                ventana_trabajador.dateNacimientoTrabajador.getText().isEmpty() ||
                ventana_trabajador.txtDniTrabajador.getText().isEmpty() ||
                ventana_trabajador.cbCargoTrabajador.getSelectedIndex() == -1 ||
                ventana_trabajador.cbTiendaTrabajador.getSelectedIndex() == -1;

    }
    /**
     * Metodo que borra los campos del cliente
     */
    private void borrarCamposCliente(){
        ventana_trabajador.txtNombreCliente.setText("");
        ventana_trabajador.txtApellidosCliente.setText("");
        ventana_trabajador.dateNacimientoCliente.setText("");
        ventana_trabajador.txtDniCliente.setText("");
        ventana_trabajador.cbGeneroCliente.setSelectedItem(-1);
        ventana_trabajador.txtCorreoCliente.setText("");
        ventana_trabajador.cbUltimaTienda.setSelectedItem(-1);
    }
    /**
     * Metodo que comprueba que algún campo esta vacio en el cliente
     * @return
     */
    private boolean comprobarClienteVacio(){
        return ventana_trabajador.txtNombreCliente.getText().isEmpty() ||
                ventana_trabajador.txtApellidosCliente.getText().isEmpty() ||
                ventana_trabajador.dateNacimientoCliente.getText().isEmpty() ||
                ventana_trabajador.txtDniCliente.getText().isEmpty() ||
                ventana_trabajador.cbGeneroCliente.getSelectedIndex() == -1 ||
                ventana_trabajador.txtCorreoCliente.getText().isEmpty() ||
                ventana_trabajador.cbUltimaTienda.getSelectedIndex() == -1;
    }
    /**
     * Metodo que borra los campos del encargo
     */
    private void borrarCamposEncargo() {
        ventana_trabajador.txtNumeroPrendasEncargo.setText("");
        ventana_trabajador.txtPrecioTotalEncargo.setText("");
        ventana_trabajador.dateEncargo.setText("");
        ventana_trabajador.dateEntregaEncargo.setText("");
        ventana_trabajador.cbClienteEncargo.setSelectedItem(-1);
    }
    /**
     * Metodo que comprueba que algún campo esta vacio en el encargo
     * @return
     */
    private boolean comprobarEncargoVacio(){
        return ventana_trabajador.txtNumeroPrendasEncargo.getText().isEmpty() ||
                ventana_trabajador.txtPrecioTotalEncargo.getText().isEmpty() ||
                ventana_trabajador.dateEncargo.getText().isEmpty() ||
                ventana_trabajador.dateEntregaEncargo.getText().isEmpty() ||
                ventana_trabajador.cbClienteEncargo.getSelectedIndex() == -1;
    }


    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    /**
     * Metodo que coge los datos del registro seleccionado en la tabla y pobla los campos con los datos
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting() && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(ventana_trabajador.tablaEmpresa.getSelectionModel())) {
                int row = ventana_trabajador.tablaEmpresa.getSelectedRow();
                ventana_trabajador.txtNombreEmpresa.setText(String.valueOf(ventana_trabajador.tablaEmpresa.getValueAt(row, 1)));
                ventana_trabajador.txtSedeEmpresa.setText(String.valueOf(ventana_trabajador.tablaEmpresa.getValueAt(row, 2)));
                ventana_trabajador.txtCantidadTiendasEmpresa.setText(String.valueOf(ventana_trabajador.tablaEmpresa.getValueAt(row, 3)));
            } else if (e.getSource().equals(ventana_trabajador.tablaDisen.getSelectionModel())) {
                int row = ventana_trabajador.tablaDisen.getSelectedRow();
                ventana_trabajador.txtNombreDisen.setText(String.valueOf(ventana_trabajador.tablaDisen.getValueAt(row, 1)));
                ventana_trabajador.txtApeDisen.setText(String.valueOf(ventana_trabajador.tablaDisen.getValueAt(row, 2)));
                ventana_trabajador.dateFechaNacDisen.setDate(Date.valueOf(String.valueOf(ventana_trabajador.tablaDisen.getValueAt(row, 3))).toLocalDate());
                ventana_trabajador.txtDniDiseniador.setText(String.valueOf(ventana_trabajador.tablaDisen.getValueAt(row, 4)));
                ventana_trabajador.txtDireccionDisen.setText(String.valueOf(ventana_trabajador.tablaDisen.getValueAt(row, 5)));
                ventana_trabajador.cbGeneroDisen.setSelectedItem(String.valueOf(ventana_trabajador.tablaDisen.getValueAt(row, 6)));
                ventana_trabajador.txtPaisDisen.setText(String.valueOf(ventana_trabajador.tablaDisen.getValueAt(row, 7)));
                ventana_trabajador.cbEmpresaDisen.setSelectedItem(String.valueOf(ventana_trabajador.tablaDisen.getValueAt(row, 8)));
            } else if (e.getSource().equals(ventana_trabajador.tablaColeccion.getSelectionModel())) {
                int row = ventana_trabajador.tablaColeccion.getSelectedRow();
                ventana_trabajador.txtNombreColeccion.setText(String.valueOf(ventana_trabajador.tablaColeccion.getValueAt(row, 1)));
                ventana_trabajador.cbMarcaColeccion.setSelectedItem(String.valueOf(ventana_trabajador.tablaColeccion.getValueAt(row, 2)));
                ventana_trabajador.cbEstacionColeccion.setSelectedItem(String.valueOf(ventana_trabajador.tablaColeccion.getValueAt(row, 3)));
                ventana_trabajador.dateCreacionColeccion.setDate(Date.valueOf(String.valueOf(ventana_trabajador.tablaColeccion.getValueAt(row, 4))).toLocalDate());
                ventana_trabajador.cbDiseniadorColeccion.setSelectedItem(String.valueOf(ventana_trabajador.tablaColeccion.getValueAt(row, 5)));
            } else if (e.getSource().equals(ventana_trabajador.tablaPrenda.getSelectionModel())) {
                int row = ventana_trabajador.tablaPrenda.getSelectedRow();
                ventana_trabajador.txtNombrePrenda.setText(String.valueOf(ventana_trabajador.tablaPrenda.getValueAt(row, 1)));
                ventana_trabajador.cbTipoPrenda.setSelectedItem(String.valueOf(ventana_trabajador.tablaPrenda.getValueAt(row, 3)));
                ventana_trabajador.cbColorPrenda.setSelectedItem(String.valueOf(ventana_trabajador.tablaPrenda.getValueAt(row, 4)));
                ventana_trabajador.sliderTallaPrenda.setValue(Integer.valueOf(String.valueOf(ventana_trabajador.tablaPrenda.getValueAt(row, 5))));
                ventana_trabajador.txtPrecioPrenda.setText(String.valueOf(ventana_trabajador.tablaPrenda.getValueAt(row, 6)));
                ventana_trabajador.cbColeccionPrenda.setSelectedItem(String.valueOf(ventana_trabajador.tablaPrenda.getValueAt(row, 7)));
            } else if (e.getSource().equals(ventana_trabajador.tablaTienda.getSelectionModel())) {
                int row = ventana_trabajador.tablaTienda.getSelectedRow();
                ventana_trabajador.txtNombreTienda.setText(String.valueOf(ventana_trabajador.tablaTienda.getValueAt(row, 1)));
                ventana_trabajador.txtDireccionTienda.setText(String.valueOf(ventana_trabajador.tablaTienda.getValueAt(row, 2)));
                ventana_trabajador.txtCiudadTienda.setText(String.valueOf(ventana_trabajador.tablaTienda.getValueAt(row, 3)));
                ventana_trabajador.sliderPlantasTienda.setValue(Integer.valueOf(String.valueOf(ventana_trabajador.tablaTienda.getValueAt(row, 4))));
            } else if(e.getSource().equals(ventana_trabajador.tablaCT.getSelectionModel())){
                int row = ventana_trabajador.tablaCT.getSelectedRow();
                ventana_trabajador.cbColeccionCT.setSelectedItem(String.valueOf(ventana_trabajador.tablaCT.getValueAt(row, 1)));
                ventana_trabajador.cbTiendaCT.setSelectedItem(String.valueOf(ventana_trabajador.tablaCT.getValueAt(row, 2)));
            } else if(e.getSource().equals(ventana_trabajador.tablaTrabajador.getSelectionModel())){
                int row = ventana_trabajador.tablaTrabajador.getSelectedRow();
                ventana_trabajador.txtNombreTrabajador.setText(String.valueOf(ventana_trabajador.tablaTrabajador.getValueAt(row, 1)));
                ventana_trabajador.txtApellidosTrabajador.setText(String.valueOf(ventana_trabajador.tablaTrabajador.getValueAt(row, 2)));
                ventana_trabajador.dateNacimientoTrabajador.setDate(Date.valueOf(String.valueOf(ventana_trabajador.tablaTrabajador.getValueAt(row,3))).toLocalDate());
                ventana_trabajador.txtDniTrabajador.setText(String.valueOf(ventana_trabajador.tablaTrabajador.getValueAt(row, 4)));
                ventana_trabajador.cbGeneroTrabajador.setSelectedItem(String.valueOf(ventana_trabajador.tablaTrabajador.getValueAt(row, 5)));
                ventana_trabajador.cbCargoTrabajador.setSelectedItem(String.valueOf(ventana_trabajador.tablaTrabajador.getValueAt(row, 6)));
                ventana_trabajador.cbTiendaTrabajador.setSelectedItem(String.valueOf(ventana_trabajador.tablaTrabajador.getValueAt(row, 7)));
            }else if(e.getSource().equals(ventana_trabajador.tablaCliente.getSelectionModel())){
                int row = ventana_trabajador.tablaCliente.getSelectedRow();
                ventana_trabajador.txtNombreCliente.setText(String.valueOf(ventana_trabajador.tablaCliente.getValueAt(row, 1)));
                ventana_trabajador.txtApellidosCliente.setText(String.valueOf(ventana_trabajador.tablaCliente.getValueAt(row, 2)));
                ventana_trabajador.dateNacimientoCliente.setDate(Date.valueOf(String.valueOf(ventana_trabajador.tablaCliente.getValueAt(row, 3))).toLocalDate());
                ventana_trabajador.txtDniCliente.setText(String.valueOf(ventana_trabajador.tablaCliente.getValueAt(row, 4)));
                ventana_trabajador.cbGeneroCliente.setSelectedItem(String.valueOf(ventana_trabajador.tablaCliente.getValueAt(row, 5)));
                ventana_trabajador.txtCorreoCliente.setText(String.valueOf(ventana_trabajador.tablaCliente.getValueAt(row, 6)));
                ventana_trabajador.cbUltimaTienda.setSelectedItem(String.valueOf(ventana_trabajador.tablaCliente.getValueAt(row, 7)));
            }else if(e.getSource().equals(ventana_trabajador.tablaEncargo.getSelectionModel())){
                int row = ventana_trabajador.tablaEncargo.getSelectedRow();
                ventana_trabajador.txtNumeroPrendasEncargo.setText(String.valueOf(ventana_trabajador.tablaEncargo.getValueAt(row, 1)));
                ventana_trabajador.txtPrecioTotalEncargo.setText(String.valueOf(ventana_trabajador.tablaEncargo.getValueAt(row, 2)));
                ventana_trabajador.dateEncargo.setDate(Date.valueOf(String.valueOf(ventana_trabajador.tablaEncargo.getValueAt(row, 3))).toLocalDate());
                ventana_trabajador.dateEntregaEncargo.setDate(Date.valueOf(String.valueOf(ventana_trabajador.tablaEncargo.getValueAt(row, 4))).toLocalDate());
                ventana_trabajador.cbClienteEncargo.setSelectedItem(String.valueOf(ventana_trabajador.tablaEncargo.getValueAt(row, 5)));
            }else if(e.getValueIsAdjusting() && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar){
                if(e.getSource().equals(ventana_trabajador.tablaEmpresa.getSelectionModel())){
                    borrarCamposEmpresa();
                } else if(e.getSource().equals(ventana_trabajador.tablaDisen.getSelectionModel())){
                    borrarCamposDisen();
                } else if(e.getSource().equals(ventana_trabajador.tablaColeccion.getSelectionModel())){
                    borrarCamposColeccion();
                } else if(e.getSource().equals(ventana_trabajador.tablaPrenda.getSelectionModel())){
                    borrarCamposPrenda();
                } else if(e.getSource().equals(ventana_trabajador.tablaTienda.getSelectionModel())){
                    borrarCamposTienda();
                } else if(e.getSource().equals(ventana_trabajador.tablaCT.getSelectionModel())) {
                    borrarCamposCT();
                } else if(e.getSource().equals(ventana_trabajador.tablaTrabajador.getSelectionModel())){
                    borrarCamposTrabajador();
                } else if(e.getSource().equals(ventana_trabajador.tablaCliente.getSelectionModel())){
                    borrarCamposCliente();
                } else if(e.getSource().equals(ventana_trabajador.tablaEncargo.getSelectionModel())){
                    borrarCamposEncargo();
                }
            }
        }
    }
}

