package gui;

import javax.swing.*;
import java.awt.*;

public class Ventana_Administrador extends JFrame{
    private final static String TITULOFRAME = "Ventana administrador";
    private JPanel panel1;
    JTextField txtUsuarioTrabajador;
    JButton btnCrearTrabajador;
    JPasswordField txtPassTrabajador;
    JMenuItem cerrarSesion;
    /**
     * Constructor de la ventana trabajador donde inicias el frame y le pones el titulo.
     */
    public Ventana_Administrador(){
        super(TITULOFRAME);
        initFrame();
    }
    /**
     * Metodo que crea todos los componentes de la ventana, tanto tamaño, localización la imagen del icono...
     */
    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(this);
        this.setLocation(750, 250);
        this.setIconImage(new ImageIcon("DREAM.png").getImage());
        this.setResizable(false);
        setMenu();
    }
    /**
     * Metodo que crea el menu.
     */
    void setMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Menu");
        cerrarSesion = new JMenuItem("Cerrar sesión");
        cerrarSesion.setActionCommand("Cerrar_sesion_admin");
        menu.add(cerrarSesion);
        barra.add(menu);
        this.setJMenuBar(barra);
    }
}
