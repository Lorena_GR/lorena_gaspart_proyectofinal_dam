package gui;

import enums.Tipo;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Ventana_Cliente extends JFrame{
    private final static String TITULOFRAME = "Ventana cliente";
    private JPanel panel1;
     JButton btnCarrito;
     JTable tablaCliente;
     JButton btnAnyadirAlCarrito;
     JComboBox comboFiltro;
     JButton btnFiltrar;
     JButton recargarCarrito;
    JMenuItem cerrarSesion;
    DefaultTableModel dtmPrendasCliente;

    /**
     * Constructor de la ventana cliente donde inicias el frame y le pones el titulo.
     */
    public Ventana_Cliente(){
        super(TITULOFRAME);
        initFrame();
    }
    /**
     * Metodo que crea todos los componentes de la ventana, tanto tamaño, localización la imagen del icono...
     */
    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(this);
        this.setLocation(500, 250);
        this.setIconImage(new ImageIcon("DREAM.png").getImage());
        this.setResizable(false);
        setMenu();
        setTableModels();
        setEnumComboBox();
    }
    /**
     * Metodo que añade los datos a los combobox
     */
    private void setEnumComboBox() {
        for (Tipo constant : Tipo.values()) {
            comboFiltro.addItem(constant.getTipo());
        }
    }
    /**
     * Metodo que crea los tablemodels
     */
    private void setTableModels(){
        this.dtmPrendasCliente = new DefaultTableModel();
        this.tablaCliente.setModel(dtmPrendasCliente);
    }
    /**
     * Metodo que crea el menu.
     */
    void setMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Menu");
        cerrarSesion = new JMenuItem("Cerrar sesión");
        cerrarSesion.setActionCommand("Cerrar_sesion_cliente");
        menu.add(cerrarSesion);
        barra.add(menu);
        this.setJMenuBar(barra);
    }
}
