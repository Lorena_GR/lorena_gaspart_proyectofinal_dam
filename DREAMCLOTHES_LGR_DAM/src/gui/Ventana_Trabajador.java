package gui;

import com.github.lgooddatepicker.components.DatePicker;
import enums.*;
import enums.Color;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Ventana_Trabajador extends JFrame {
     private final static String TITULOFRAME = "Ventana trabajador";
     JMenuItem cerrarSesion;
     JMenuItem informe_empresas;
     JMenuItem informe_diseniadores;
     JMenuItem informe_prendas;
     JMenuItem informe_colecciones;
     JMenuItem graficoTabla;
     JMenuItem graficoQueso;
     JPanel panel1;
     JTabbedPane tabbedPane1;
     // Empresa
     JTextField txtNombreEmpresa;
     JTextField txtSedeEmpresa;
     JTextField txtCiudadEmpresa;
     JTextField txtCantidadTiendasEmpresa;
     JButton btnAnyadirEmpresa;
     JButton btnModificarEmpresa;
     JButton btnEliminarEmpresa;
     JButton btnBuscarEmpresa;
     JTable tablaEmpresa;
     // Diseñador
     JTextField txtNombreDisen;
     JTextField txtApeDisen;
     JTextField txtDireccionDisen;
     JComboBox cbGeneroDisen;
     JTextField txtPaisDisen;
     JButton btnAnyadirDisen;
     JButton btnModificarDisen;
     JButton btnEliminarDisen;
     JButton btnBuscarDisen;
     JTable tablaDisen;
     JComboBox cbEmpresaDisen;
     DatePicker dateFechaNacDisen;
     // Coleccion
     JTextField txtNombreColeccion;
     JComboBox cbMarcaColeccion;
     JComboBox cbEstacionColeccion;
     JButton btnAnyadirColeccion;
     JButton btnModificarColeccion;
     JButton btnEliminarColeccion;
     JButton btnBuscarColeccion;
     JTable tablaColeccion;
     DatePicker dateCreacionColeccion;
     JComboBox cbDiseniadorColeccion;
     // Prenda
     JRadioButton mujerRadioButton;
     JRadioButton hombreRadioButton;
     JRadioButton ninioRadioButton;
     JRadioButton niniaRadioButton;
     JComboBox cbColorPrenda;
     JSlider sliderTallaPrenda;
     JTextField txtPrecioPrenda;
     JTextField txtNombrePrenda;
     JButton btnAnyadirPrenda;
     JButton btnModificarPrenda;
     JButton btnEliminarPrenda;
     JButton btnBuscarPrenda;
     JTable tablaPrenda;
     JComboBox cbColeccionPrenda;
     JComboBox cbTipoPrenda;
    // Tienda
     JTextField txtNombreTienda;
     JTextField txtDireccionTienda;
     JSlider sliderPlantasTienda;
     JButton btnAnyadirTienda;
     JButton btnModificarTienda;
     JButton btnEliminarTienda;
     JButton btnBuscarTienda;
     JTable tablaTienda;
     JTextField txtCiudadTienda;
    // Trabajador
     JTextField txtNombreTrabajador;
     JTextField txtApellidosTrabajador;
     JComboBox cbCargoTrabajador;
     JButton btnAnyadirTrabajador;
     JButton btnModificarTrabajador;
     JButton btnEliminarTrabajador;
     JButton btnBuscarTrabajador;
     JTable tablaTrabajador;
     DatePicker dateNacimientoTrabajador;
     JComboBox cbTiendaTrabajador;
     JComboBox cbGeneroTrabajador;
     // Encargo
     JTextField txtNumeroPrendasEncargo;
     JTextField txtPrecioTotalEncargo;
     JButton btnAnyadirEncargo;
     JButton btnModificarEncargo;
     JButton btnEliminarEncargo;
     JButton btnBuscarEncargo;
     JTable tablaEncargo;
     JComboBox cbClienteEncargo;
    DatePicker dateEntregaEncargo;
     // Cliente
     JTextField txtNombreCliente;
     JTextField txtApellidosCliente;
     JComboBox cbGeneroCliente;
     JTextField txtCorreoCliente;
     JComboBox cbUltimaTienda;
     JButton btnAnyadirCliente;
     JButton btnModificarCliente;
     JButton btnEliminarCliente;
     JButton btnBuscarCliente;
     JTable tablaCliente;
     DatePicker dateNacimientoCliente;
     DatePicker dateEncargo;
     JTextField txtDniDiseniador;
     JTextField txtDniTrabajador;
     JTextField txtDniCliente;
     JComboBox cbColeccionCT;
     JComboBox cbTiendaCT;
     JButton btnAniadirColTienda;
     JButton btnModificarColTienda;
     JButton btnEliminarColTienda;
     JButton btnBuscarColTienda;
     JTable tablaCT;
     JButton recargarEmpresa;
     JButton recargarCliente;
     JButton recargarEncargo;
     JButton recargarTrabajador;
     JButton recargarCT;
     JButton recargarTienda;
     JButton recargarPrenda;
     JButton recargarColeccion;
     JButton recargarDiseniador;
    // Default Table Model
     DefaultTableModel dtmEmpresa;
     DefaultTableModel dtmDiseniador;
     DefaultTableModel dtmColeccion;
     DefaultTableModel dtmPrenda;
     DefaultTableModel dtmTienda;
     DefaultTableModel dtmCT;
     DefaultTableModel dtmTrabajador;
     DefaultTableModel dtmCliente;
     DefaultTableModel dtmEncargo;

    /**
     * Constructor de la ventana trabajador donde inicias el frame y le pones el titulo.
     */
    public Ventana_Trabajador(){
        super(TITULOFRAME);
        initFrame();
    }
    /**
     * Metodo que crea todos los componentes de la ventana, tanto tamaño, localización la imagen del icono...
     */
     private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(this);
        this.setIconImage(new ImageIcon("DREAM.png").getImage());
        this.setLocation(575, 250);
        this.setResizable(false);
        setTableModels();
        setMenu();
        setEnumComboBox();
    }
    /**
     * Metodo que crea los tablemodels
     */
    private void setTableModels(){
        this.dtmEmpresa = new DefaultTableModel();
        this.tablaEmpresa.setModel(dtmEmpresa);
        this.dtmDiseniador = new DefaultTableModel();
        this.tablaDisen.setModel(dtmDiseniador);
        this.dtmColeccion = new DefaultTableModel();
        this.tablaColeccion.setModel(dtmColeccion);
        this.dtmPrenda = new DefaultTableModel();
        this.tablaPrenda.setModel(dtmPrenda);
        this.dtmTienda = new DefaultTableModel();
        this.tablaTienda.setModel(dtmTienda);
        this.dtmCT = new DefaultTableModel();
        this.tablaCT.setModel(dtmCT);
        this.dtmTrabajador = new DefaultTableModel();
        this.tablaTrabajador.setModel(dtmTrabajador);
        this.dtmCliente = new DefaultTableModel();
        this.tablaCliente.setModel(dtmCliente);
        this.dtmEncargo = new DefaultTableModel();
        this.tablaEncargo.setModel(dtmEncargo);
}
    /**
     * Metodo que crea el menu.
     */
    private void setMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Menu");
        JMenu submenu = new JMenu("Informes");
        cerrarSesion = new JMenuItem("Cerrar sesión");
        cerrarSesion.setActionCommand("Cerrar_sesion_trabajador");
        informe_empresas = new JMenuItem("Informe empresas");
        informe_empresas.setActionCommand("Informe_empresas");
        informe_diseniadores = new JMenuItem("Informe diseñadores");
        informe_diseniadores.setActionCommand("Informe_diseniadores");
        informe_colecciones = new JMenuItem("Informe colecciones");
        informe_colecciones.setActionCommand("Informe_colecciones");
        informe_prendas = new JMenuItem("Informe prendas");
        informe_prendas.setActionCommand("Informe_prendas");
        graficoTabla = new JMenuItem("Tabla");
        graficoTabla.setActionCommand("Tabla");
        graficoQueso = new JMenuItem("Grafico redondo");
        graficoQueso.setActionCommand("Redondo");
        menu.add(cerrarSesion);
        submenu.add(informe_empresas);
        submenu.add(informe_diseniadores);
        submenu.add(informe_colecciones);
        submenu.add(informe_prendas);
        submenu.add(graficoTabla);
        submenu.add(graficoQueso);
        menu.add(submenu);
        barra.add(menu);
        this.setJMenuBar(barra);
    }

    /**
     * Metodo que añade los datos a los combobox
     */
    private void setEnumComboBox(){
        for(Cargo constant: Cargo.values()){
            cbCargoTrabajador.addItem(constant.getCargo());
        }
        for(Color constant: Color.values()){
            cbColorPrenda.addItem(constant.getColor());
        }
        for(Estacion constant: Estacion.values()){
            cbEstacionColeccion.addItem(constant.getEstacion());
        }
        for(Genero constant: Genero.values()){
            cbGeneroCliente.addItem(constant.getGenero());
            cbGeneroDisen.addItem(constant.getGenero());
            cbGeneroTrabajador.addItem(constant.getGenero());
        }
        for(Marca constant: Marca.values()){
            cbMarcaColeccion.addItem(constant.getMarca());
        }
        for(Tipo constant: Tipo.values()){
            cbTipoPrenda.addItem(constant.getTipo());
        }
    }
}

