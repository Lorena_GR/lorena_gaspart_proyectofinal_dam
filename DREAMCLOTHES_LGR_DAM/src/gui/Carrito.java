package gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Carrito extends JFrame{
    private final static String TITULOFRAME = "Carrito";
     JPanel panel1;
     JButton btnPagar;
     JTable tableCarrito;
     JButton btnEliminarProducto;
     JButton btnEliminarTodo;
     JLabel labelPrecio;
    DefaultTableModel dtmCarrito;
    JMenuItem salirCarrito;

    /**
     * Constructor de la ventana carrito donde inicias el frame y le pones el titulo.
     */
    public Carrito(){
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Metodo que crea todos los componentes de la ventana, tanto tamaño, localización la imagen del icono...
     */
    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(this);
        this.setLocation(1200, 250);
        this.setIconImage(new ImageIcon("DREAM.png").getImage());
        this.setResizable(false);
        setTableModels();
        setMenu();
    }

    /**
     * Metodo que crea los tablemodels
     */
    private void setTableModels(){
        this.dtmCarrito = new DefaultTableModel();
        this.tableCarrito.setModel(dtmCarrito);
    }

    /**
     * Metodo que crea el menu.
     */
    void setMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Menu");
        salirCarrito = new JMenuItem("Salir del carrito");
        salirCarrito.setActionCommand("Salir_carrito");
        menu.add(salirCarrito);
        barra.add(menu);
        this.setJMenuBar(barra);
    }

}
