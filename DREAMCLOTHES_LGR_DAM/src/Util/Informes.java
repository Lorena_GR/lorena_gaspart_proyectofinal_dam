package Util;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Informes {

    public static class Conexion {

        public static Connection getMySQLConexion() {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                Connection conn = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/dream_clothes","root", "");
                return conn;

            } catch (ClassNotFoundException | SQLException ex ) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE,null, ex);
            }

            return null;
        }
    }

    public static void informe(String nombre, String pdf){
        JasperPrint informe = ReporGenerator.generarInforme(nombre);

        JasperViewer viewer = new JasperViewer(informe);
        viewer.setVisible(true);
        try {
            JasperExportManager.exportReportToPdfFile(informe, pdf);
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    public static class ReporGenerator {


        public static JasperPrint generarInforme(String nombre) {

            try {
                JasperPrint informeLleno = JasperFillManager.fillReport
                        (nombre ,new HashMap(), Conexion.getMySQLConexion());
                return informeLleno;
            } catch (JRException e) {
                e.printStackTrace();
            }
            return null;
        }
    }






}