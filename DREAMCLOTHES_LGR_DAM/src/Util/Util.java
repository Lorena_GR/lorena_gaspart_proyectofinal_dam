package Util;

import javax.swing.*;

public class Util {
    /**
     * Metodo que muestra un mensaje de error
     * @param message
     */
    public static void errorAlerta(String message){
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Metodo que muestra un mensaje de aviso
     * @param message
     */
    public static void avisoAlerta(String message){
        JOptionPane.showMessageDialog(null, message, "Aviso", JOptionPane.WARNING_MESSAGE);
    }

}
