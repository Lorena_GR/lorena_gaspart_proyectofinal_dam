package pruebas;

import gui.Modelo;
import org.junit.jupiter.api.Test;

import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.*;

class ConexionTest {
    Modelo modelo = new Modelo();
    @Test
    public void conectar() throws Exception{
        Connection result = modelo.conectar();
        assertEquals(result != null, true);
    }

    @Test
    void desconectar() {
        modelo.conectar();
        Connection result = modelo.desconectar();
        assertEquals(result != null, false);
    }
}