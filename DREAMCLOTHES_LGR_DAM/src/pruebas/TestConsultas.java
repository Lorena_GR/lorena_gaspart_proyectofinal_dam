package pruebas;

import gui.Modelo;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class TestConsultas {
    Modelo modelo = new Modelo();

    @Test
    void insertarCuentaCliente() throws SQLException {
        modelo.conectar();
        modelo.insertarCuentaCliente("Antonio", "12345678");
        ResultSet resultSet = modelo.consultarUsuarioPorContrasenia("12345678");
        resultSet.next();
        assertEquals(resultSet.getString("contraseña"), "12345678");
    }

    @Test
    void insertarCuentaTrabajador() throws SQLException {
        modelo.conectar();
        modelo.insertarCuentaCliente("Pepe", "1234");
        ResultSet resultSet = modelo.consultarUsuarioPorContrasenia("1234");
        resultSet.next();
        assertEquals(resultSet.getString("contraseña"), "1234");
    }

    @Test
    void insertarEmpresa() throws SQLException {
        modelo.conectar();
        modelo.insertarEmpresa("Empresa 1", "Zaragoza", 120);
        ResultSet resultado = modelo.consultarEmpresaPorBusqueda("Empresa 1");
        resultado.next();
        assertEquals(resultado.getString("nombre"), "Empresa 1");
    }

    @Test
    void modificarEmpresa() throws SQLException {
        modelo.conectar();
        modelo.modificarEmpresa("Empresa 2", "Zaragoza", 120, 16);
        ResultSet resultado = modelo.consultarEmpresaPorBusqueda("Empresa 2");
        resultado.next();
        assertEquals(resultado.getString("nombre"), "Empresa 2");
    }

    @Test
    void eliminarEmpresa() throws SQLException {
        modelo.conectar();
        modelo.eliminarEmpresa(16);
        ResultSet resultado = modelo.consultarEmpresaPorBusqueda("Empresa 2");
        resultado.next();
        assertEquals(resultado != null, true);
    }

    @Test
    void consultarEmpresaPorBusqueda() throws SQLException {
        modelo.conectar();
        ResultSet resultado = modelo.consultarEmpresaPorBusqueda("Inditex");
        resultado.next();
        assertEquals(resultado.getString("nombre"), "Inditex");

    }

    @Test
    void insertarDiseniador() throws SQLException {
        modelo.conectar();
        modelo.insertarDiseniador("L", "G", Date.valueOf("1999-04-12"), "12345678P", "A", "Femenino", "España", "1");
        ResultSet resultado = modelo.consultarDiseniadorPorBusqueda("1");
        resultado.last();
        assertEquals(resultado.getString("dni"), "12345678P");
    }

    @Test
    void modificarDiseniador() throws SQLException {
        modelo.conectar();
        modelo.modificarDiseniador("L", "GR", Date.valueOf("1999-04-12"), "12345678P", "A", "Femenino", "España", "1", 12);
        ResultSet resultado = modelo.consultarDiseniadorPorBusqueda("1");
        resultado.last();
        assertEquals(resultado.getString("apellidos"), "Gutierrez");
    }

    @Test
    void eliminarDiseniador() throws SQLException {
        modelo.conectar();
        modelo.eliminarDiseniador(12);
        ResultSet resultado = modelo.consultarDiseniadorPorBusqueda("1");
        resultado.last();
        assertEquals(resultado != null, true);
    }

    @Test
    void consultarDiseniadorPorBusqueda() throws SQLException {
        modelo.conectar();
        ResultSet resultSet = modelo.consultarDiseniadorPorBusqueda("1");
        resultSet.next();
        assertEquals(resultSet.getString("nombre"), "Roberto");
    }

    @Test
    void insertarColeccion() throws SQLException {
        modelo.conectar();
        modelo.insertarColeccion("L", "A", "Verano", Date.valueOf("1999-12-12"), "1");
        ResultSet resultado = modelo.consultarColeccionPorBusqueda("1");
        resultado.last();
        assertEquals(resultado.getString("nombre"), "L");
    }

    @Test
    void modificarColeccion() throws SQLException {
        modelo.conectar();
        modelo.modificarColeccion("L", "A", "Invierno", Date.valueOf("1999-12-12"), "1", 25);
        ResultSet resultado = modelo.consultarColeccionPorBusqueda("1");
        resultado.last();
        assertEquals(resultado.getString("estacion"), "Invierno");
    }

    @Test
    void eliminarColeccion() throws SQLException {
        modelo.conectar();
        modelo.eliminarColeccion(25);
        ResultSet resultado = modelo.consultarColeccionPorBusqueda("1");
        resultado.last();
        assertEquals(resultado != null, true);
    }

    @Test
    void consultarColeccionPorBusqueda() throws SQLException {
        modelo.conectar();
        ResultSet resultSet = modelo.consultarColeccionPorBusqueda("1");
        resultSet.last();
        assertEquals(resultSet.getString("marca"), "Rosa Clara");
    }

    @Test
    void insertarPrenda() throws SQLException {
        modelo.conectar();
        modelo.insertarPrenda("L", "Mujer", "Vestido", "Rosa", 40, Float.parseFloat("20.20"), "1");
        ResultSet resultado = modelo.consultarPrendaPorBusqueda("1");
        resultado.last();
        assertEquals(resultado.getString("nombre"), "L");
    }

    @Test
    void modificarPrenda() throws SQLException {
        modelo.conectar();
        modelo.modificarPrenda("L", "Hombre", "Vestido", "Rosa", 40, Float.parseFloat("20.20"), "1", 26);
        ResultSet resultado = modelo.consultarPrendaPorBusqueda("1");
        resultado.last();
        assertEquals(resultado.getString("persona"), "Hombre");
    }

    @Test
    void eliminarPrenda() throws SQLException {
        modelo.conectar();
        modelo.eliminarPrenda(26);
        ResultSet resultado = modelo.consultarPrendaPorBusqueda("1");
        resultado.last();
        assertEquals(resultado != null, true);
    }

    @Test
    void consultarPrendaPorBusqueda() throws SQLException {
        modelo.conectar();
        ResultSet resultado = modelo.consultarPrendaPorBusqueda("1");
        resultado.next();
        assertEquals(resultado.getString("nombre"), "Prenda 1");

    }
}