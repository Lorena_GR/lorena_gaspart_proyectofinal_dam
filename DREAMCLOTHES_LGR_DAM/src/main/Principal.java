package main;

import gui.Controlador;
import gui.Inicio_Sesion;
import gui.Modelo;

public class Principal {
    public static void main(String[] args) {
        Inicio_Sesion inicio_sesion = new Inicio_Sesion();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(inicio_sesion, modelo);
    }
}
